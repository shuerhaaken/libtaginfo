**Libtaginfo** is a library for reading media metadata (tags).


**Libtaginfo** is a convenience wrapper for [taglib](http://taglib.github.com/) with **C** and **vala** bindings.

Features are reading/writing fields like: **Artist, Album, Title, Genre, AlbumArtist, Comments, Disk number,  Compilation flag, User labels, Embedded Images, Lyrics, Audio properties (length, bitrate, samplerate, channels ...), ...**

**Libtaginfo is fast**. There is not much overhead coming with this library, so it reads at almost the same speed as taglib itself, which is the fastest way around to read tag information.

Additionally, this library offers **C and vala bindings** that provide a lot of features that are not available via TagLib's own C bindings (and the according vala bindings). 

**Documentation** for Libtaginfo is available via DOxygen and created during the build.

***

The lacking possibility of accessing certain data fields via taglibs own C bindings was the original motivation for the creation of this library.

libtaginfo is distributed under the **GNU Lesser General Public License (LGPLv2+)**. This was possible due to a code donation of J. Rios.

Libtaginfo can be used from **C++, C and vala** so far.

***

Supported files: Mp3, Ogg, Flac, Opus, MPC, Ape, Speex, WavPack, TrueAudio, WAV, AIFF, MP4 and ASF.

***

 - There is a big number of unit tests covering the full API of libtaginfo and some tests for the C API.

 - The library is prooved to be useful and working by a wide user base in quite some linux distributions.

 - Some **packaging data** for Ubuntu and for Debian is available via a subfolder of the project.

 - An **Ubuntu PPA** containing the latest release of libtaginfo can be found [here](https://launchpad.net/~shkn/+archive/xnoise).

***

**PLEASE HELP IMPROVING LIBTAGINFO!**

**I would appreciate to collect patches for this library** and improve it for the benefit of everybody that needs a convenient, fast (and toolkit independent) C / Vala API for tag reading/writing. There is a **TODO** file in the project folder listing some things that need to be done.

***

 
 - Parts of the C++ code were donated by J. Rios under the LGPLv2.
 
 - This library, its C and vala bindings and the documentation are maintained by shuerhaaken.

***

If you have questions please ask on the [xnoise mailing list](http://groups.google.com/group/xnoise) !


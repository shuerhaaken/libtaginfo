/*
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Matias De lellis <mati86dl@gmail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */


#include "taginfo.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "taginfo_c.h"


using namespace TagInfo;
using namespace TagLib;


//*****************STRING MANAGEMENT*****************************************************
static List<char *> strings;
static bool stringManagementEnabled = false;

//! Enable string management for libtaginfo
void taginfo_info_enable_string_management(BOOL management) {
    stringManagementEnabled = bool(management);
}

//! Clean up used strings
//! To be used in case string management is used
void taginfo_info_free_strings() {
    if(!stringManagementEnabled)
        return;
    
    for(List<char *>::Iterator it = strings.begin(); it != strings.end(); ++it)
        free(*it);
    strings.clear();
}
//*****************/STRING MANAGEMENT****************************************************

static char **string_list_to_c_array(const StringList &list, int *out_length) {
    if(list.isEmpty()) {
        //cout << "list not there"  << endl;
        return NULL;
    }
    uint len = list.size();
    char **array = (char**)malloc(len * sizeof(char*));
    int j = 0;
    for(StringList::ConstIterator it = list.begin(); it != list.end(); ++it) {
        array[j] = ::strdup((*it).toCString(false));
        if(stringManagementEnabled && array[j])
            strings.prepend(array[j]);
        j++;
    }
    *out_length = len;
    return array;
}

static StringList string_list_from_c_array(const char* const* array, int length) {
    StringList list;
    for(int j = 0; j < length; j++) {
        if(array[j]) {
            list.append(array[j]);
        }
    }
    return list;
}


//*****************FACTORY CREATION******************************************************
/*!
 * Creates a TagInfo_Info object based on \a filename.  TagInfo will try to guess the file
 * type.
 *
 * \return NULL if the file type cannot be determined or the file cannot
 * be opened. The returned object should be freed with taginfo_info_free() after usage
 */
TagInfo_Info * taginfo_info_create(const char *filename) {
    return reinterpret_cast<TagInfo_Info *>(Info::create(filename));
}

/*!
 * Creates a TagInfo_Info object based on \a filename and TagInfo_MediaFileType
 *
 * \return NULL if the file type cannot be determined or the file cannot
 * be opened. The returned object should be freed with taginfo_info_free() after usage
 */
TagInfo_Info * taginfo_info_create_with_file_type(const char *filename,
                                                     TagInfo_MediaFileType media_file_type) {
    return reinterpret_cast<TagInfo_Info *>(Info::create_with_file_type(filename, 
                                                                              (MediaFileType)media_file_type)
    );
}

/*!
 * Creates a TagInfo_Info object based on \a filename and mimetype
 *
 * \return NULL if the file type cannot be determined or the file cannot
 * be opened. The returned object should be freed with taginfo_info_free() after usage
 */
TagInfo_Info * taginfo_info_create_from_mime(const char *filename, const char *mime_type) {
    return reinterpret_cast<TagInfo_Info *>(Info::create_from_mime(filename, 
                                                                            mime_type)
    );
}
//*****************/FACTORY CREATION*****************************************************

BOOL  taginfo_info_is_valid(TagInfo_Info *info) {
    Info *i = reinterpret_cast<Info *>(info);
    return i != NULL && i->is_valid();
}

//*****************GENERAL MEMORY MANAGEMENT*********************************************
/*!
 * Frees a TagInfo_Info object.
 */
//! \param info A TagInfo_Info object
void taginfo_info_free(TagInfo_Info *info) {
    delete reinterpret_cast<Info *>(info);
}
//*****************/GENERAL MEMORY MANAGEMENT********************************************


//*****************TAG READ/WRITE API****************************************************
//! Load the tag information from the media file to the TagInfo_Info object
//! \param info A TagInfo_Info object
BOOL taginfo_info_load(TagInfo_Info *info) {
    Info *i = reinterpret_cast<Info *>(info);
    return i->load();
}
//! Save the tag information from the TagInfo_Info object to the media file.
//! \param info A TagInfo_Info object
BOOL taginfo_info_save(TagInfo_Info *info) {
    Info *i = reinterpret_cast<Info *>(info);
    return i->save();
}

//! Get the artist tag of a media track.
/*!
\return The artist as char *. In case the string management is not enabled, the caller should free this string
*/
//! \param info A TagInfo_Info object
char * taginfo_info_get_artist(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String artist = i->get_artist();
    char *s = ::strdup(artist.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the artist tag of a media track.
//! \param info A TagInfo_Info object
/*!
\param artist Artist as const char *.
*/
void taginfo_info_set_artist(TagInfo_Info *info, const char *artist) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_artist(String(artist, String::UTF8));
}

//! Get the original artist of a media track.
//! \param info A TagInfo_Info object
/*!
\return The original artist as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_original_artist (const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String artist = i->get_original_artist ();
    char *s = ::strdup(artist.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the original artist of a media track.
//! \param info A TagInfo_Info object
/*!
\param artist Original artist as const char *.
*/
void taginfo_info_set_original_artist (TagInfo_Info *info, const char *artist) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_original_artist (String(artist, String::UTF8));
}

//! Get the album of a media track.
//! \param info A TagInfo_Info object
/*!
\return The album as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_album(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String album = i->get_album();
    char *s = ::strdup(album.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the album of a media track.
//! \param info A TagInfo_Info object
/*!
\param album Album as const char *.
*/
void taginfo_info_set_album(TagInfo_Info *info, const char *album) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_album(String(album, String::UTF8));
}

//! Get the title of a media track.
//! \param info A TagInfo_Info object
/*!
\return The title as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_title(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String title = i->get_title();
    char *s = ::strdup(title.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the title of a media track.
//! \param info A TagInfo_Info object
/*!
\param title Title as const char *.
*/
void taginfo_info_set_title(TagInfo_Info *info, const char *title) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_title(String(title, String::UTF8));
}

//! Get the albumartist tag of a media track.
//! \param info A TagInfo_Info object
/*!
\return The albumartist as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_albumartist(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String album_artist = i->get_album_artist();
    char *s = ::strdup(album_artist.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the album artist tag of a media track.
//! \param info A TagInfo_Info object
/*!
\param albumartist Album artist as const char *.
*/
void taginfo_info_set_albumartist(TagInfo_Info *info, const char *albumartist) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_album_artist(String(albumartist, String::UTF8));
}

//! Get the comment tag of a media track.
//! \param info A TagInfo_Info object
/*!
\return The comment as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_comment(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String comments = i->get_comments();
    char *s = ::strdup(comments.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the comment of a media track.
//! \param info A TagInfo_Info object
/*!
\param comment Comment as const char *.
*/
void taginfo_info_set_comment(TagInfo_Info *info, const char *comment) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_comments(String(comment, String::UTF8));
}

//! Get the composer tag of a media track.
//! \param info A TagInfo_Info object
/*!
\return The composer as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_composer(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String composer = i->get_composer();
    char *s = ::strdup(composer.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the composer of a media track.
//! \param info A TagInfo_Info object
/*!
\param composer Composer as const char *.
*/
void taginfo_info_set_composer(TagInfo_Info *info, const char *composer) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_composer(String(composer, String::UTF8));
}

//! Get the homepage tag of a media track.
//! \param info A TagInfo_Info object
/*!
\return The homepage as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_homepage(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String homepage = i->get_homepage();
    char *s = ::strdup(homepage.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the homepage tag of a media track.
//! \param info A TagInfo_Info object
/*!
\param homepage Homepage tag as const char *.
*/
void taginfo_info_set_homepage(TagInfo_Info *info, const char *homepage) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_homepage(String(homepage, String::UTF8));
}

//! Get the encoder tag of a media track.
//! \param info A TagInfo_Info object
/*!
\return The encoder as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_encoder(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String encoder = i->get_encoder();
    char *s = ::strdup(encoder.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the encoder of a media track.
//! \param info A TagInfo_Info object
/*!
\param encoder Encoder tag as const char *.
*/
void taginfo_info_set_encoder(TagInfo_Info *info, const char *encoder) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_encoder(String(encoder, String::UTF8));
}

//! Get the copyright tag of a media track.
//! \param info A TagInfo_Info object
/*!
\return The copyright as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_copyright(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String copyright = i->get_copyright();
    char *s = ::strdup(copyright.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the copyright tag of a media track.
//! \param info A TagInfo_Info object
/*!
\param copyright Copyright tag as const char *.
*/
void taginfo_info_set_copyright(TagInfo_Info *info, const char *copyright) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_copyright(String(copyright, String::UTF8));
}

//! Get the genre tag of a media track.
//! \param info A TagInfo_Info object
/*!
\return The genre as char *. In case the string management is not enabled, the caller should free this string
*/
char * taginfo_info_get_genre(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String genre = i->get_genre();
    char *s = ::strdup(genre.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}
//! Set the genre tag of a media track.
//! \param info A TagInfo_Info object
/*!
\param genre Genre tag as const char *.
*/
void taginfo_info_set_genre(TagInfo_Info *info, const char *genre) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_genre(String(genre, String::UTF8));
}

//! Get the track number of a media track.
//! \param info A TagInfo_Info object
/*!
\return The track number as int.
*/
int taginfo_info_get_track_number(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_track_number();
}
//! Set the track number tag of a media track.
//! \param info A TagInfo_Info object
/*!
\param track_number Track number tag as int.
*/
void taginfo_info_set_track_number(TagInfo_Info *info, int track_number) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_track_number(track_number);
}

//! Get the track count of a media track.
//! \param info A TagInfo_Info object
/*!
\return The track count as int.
*/
int taginfo_info_get_track_count(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_track_count();
}
//! Set the track count tag of a media track.
//! \param info A TagInfo_Info object
/*!
\param track_count Track count tag as int.
*/
void taginfo_info_set_track_count(TagInfo_Info *info, int track_count) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_track_count(track_count);
}

//! Get the publishing year of a media track.
//! \param info A TagInfo_Info object
/*!
\return The year as int.
*/
int taginfo_info_get_year(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_year();
}
//! Set the year tag of a media track.
//! \param info A TagInfo_Info object
/*!
\param year Year tag as int.
*/
void taginfo_info_set_year(TagInfo_Info *info, int year) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_year(year);
}

/*!
Get the bitrate audio property
\param info A TagInfo_Info object
\return The bitrate as int.
*/
int taginfo_info_get_bitrate(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_bitrate();
}

/*!
Get the samplerate audio property
\param info A TagInfo_Info object
\return The samplerate as int.
*/
int taginfo_info_get_samplerate(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_samplerate();
}

/*!
Get the channels audio property
\param info A TagInfo_Info object
\return The number of channels as int.
*/
int taginfo_info_get_channels(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_channels();
}

/*!
Get the length audio property
\param info A TagInfo_Info object
\return The length in seconds as int.
*/
int taginfo_info_get_length(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_length_seconds();
}

//! Get information on the availability of an image that is embedded in the media file.
/*!
\param info A TagInfo_Info object
\return TRUE if there is an image
*/
BOOL taginfo_info_get_has_image(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    //bool image_available = i->get_has_image();
    return i->get_has_image();
}

//! Get the volume number of a media track.
/*!
\param info A TagInfo_Info object
\return The volume number as int.
*/
int taginfo_info_get_volume_number(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_volume_number();
}
//! Set the volume number tag of a media track.
/*!
\param info A TagInfo_Info object
\param number Volume number tag as int.
*/
void taginfo_info_set_volume_number(TagInfo_Info *info, int number) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_volume_number(number);
}


//! Get the BPM of a media track.
/*!
\param info A TagInfo_Info object
\return The BPM as int.
*/
int  taginfo_info_get_beats_per_minute(const TagInfo_Info *info) {
    const Info * i = reinterpret_cast<const Info *>(info);
    return i->get_beats_per_minute();
}
//! Set the BPM of a media track.
/*!
\param info A TagInfo_Info object
\param new_bpm BPM as int.
*/
void taginfo_info_set_beats_per_minute(TagInfo_Info *info, int new_bpm) {
    Info * i = reinterpret_cast<Info *>(info);
    i->set_beats_per_minute(new_bpm);
}

//! Get the volume count of a media track.
/*!
\param info A TagInfo_Info object
\return The volume_count as int.
*/
int taginfo_info_get_volume_count(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_volume_count();
}
//! Set the volume count tag of a media track.
/*!
\param info A TagInfo_Info object
\param count Volume count tag as int.
*/
void taginfo_info_set_volume_count(TagInfo_Info *info, int count) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_volume_count(count);
}

//! Get the rating of a media track. rating 1 - 5; 0 -> not set
/*!
\param info A TagInfo_Info object
\return The rating as int.
*/
int taginfo_info_get_rating(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_rating();
}
//! Set the rating tag of a media track. rating 1 - 5; 0 -> not set
/*!
\param info A TagInfo_Info object
\param rating Rating as int.
*/
void taginfo_info_set_rating(TagInfo_Info *info, int rating) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_rating(rating);
}

//! Get the play count of a media track.
/*!
\param info A TagInfo_Info object
\return The play count as int.
*/
int  taginfo_info_get_playcount(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    return i->get_playcount();
}
//! Set the play count tag of a media track.
/*!
\param info A TagInfo_Info object
\param count Play count tag as int.
*/
void taginfo_info_set_playcount(TagInfo_Info *info, int count) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_playcount(count);
}

//! Get the information if the media file is part of a compilation / VA album.
/*!
\param info A TagInfo_Info object
\return TRUE if it is part of a compilation
*/
BOOL taginfo_info_get_is_compilation(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    BOOL s = i->get_is_compilation();
    return s;
}
//! Set the information if the media file is part of a compilation / VA album.
/*!
\param info A TagInfo_Info object
\param is_compilation TRUE if it is part of a compilation
*/
void taginfo_info_set_is_compilation(TagInfo_Info *info, BOOL is_compilation) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_is_compilation((bool)is_compilation);
}

//! Get an array of user defined track labels.
/*!
\param info A TagInfo_Info object
\return Array of track labels
\param labels_length The return location for the length of the returned array.
*/
char **taginfo_info_get_track_labels(const TagInfo_Info *info,
                                     int *labels_length) {
    const Info *i = reinterpret_cast<const Info *>(info);
    StringList list = i->get_track_labels_list();
    return string_list_to_c_array(list, labels_length);
}
//! Set an array of user defined track labels.
/*!
\param info A TagInfo_Info object
\param labels An array of user defined track labels
\param labels_length The length of the array
*/
void taginfo_info_set_track_labels(TagInfo_Info *info,
                                   const char* const* labels, int labels_length) {
    Info *i = reinterpret_cast<Info *>(info);
    StringList list = string_list_from_c_array(labels, labels_length);
    i->set_track_labels_list(list);
}

//! Get an array of user defined album labels.
/*!
\param info A TagInfo_Info object
\return Array of album labels
\param labels_length The return location for the length of the returned array.
*/
char **taginfo_info_get_album_labels(const TagInfo_Info *info,
                                     int *labels_length) {
    const Info *i = reinterpret_cast<const Info *>(info);
    StringList list = i->get_album_labels_list();
    return string_list_to_c_array(list, labels_length);
}
//! Set an array of user defined album labels.
/*!
\param info A TagInfo_Info object
\param labels An array of user defined album labels
\param labels_length The length of the array
*/
void taginfo_info_set_album_labels(TagInfo_Info *info,
                                   const char* const* labels, int labels_length) {
    Info *i = reinterpret_cast<Info *>(info);
    StringList list = string_list_from_c_array(labels, labels_length);
    i->set_album_labels_list(list);
}

//! Get an array of user defined artist labels.
/*!
\param info A TagInfo_Info object
\return Array of artist labels
\param labels_length The return location for the length of the returned array.
*/
char** taginfo_info_get_artist_labels(const TagInfo_Info *info,
                                      int * labels_length) {
    const Info *i = reinterpret_cast<const Info *>(info);
    StringList list = i->get_artist_labels_list();
    return string_list_to_c_array(list, labels_length);
}
//! Set an array of user defined artist labels.
/*!
\param info A TagInfo_Info object
\param labels An array of user defined artist labels
\param labels_length The length of the array
*/
void taginfo_info_set_artist_labels(TagInfo_Info *info,
                                    const char* const* labels, int labels_length) {
    Info *i = reinterpret_cast<Info *>(info);
    StringList list = string_list_from_c_array(labels, labels_length);
    i->set_artist_labels_list(list);
}

//****************/TAG READ/WRITE API****************************************************




Image ** image_list_to_image_array(const ImageList &images, int &image_count) {
    image_count = 0;
    if(0 < images.size()) {
        Image ** image_array = new Image*[images.size()];
        int i = 0;
        for(ImageList::ConstIterator it = images.begin(); it != images.end(); ++it) {
            image_array[i] = *it;
            image_count++;
        }
        return image_array;
    }
    return NULL;
}

//*****************IMAGES****************************************************************
//! Get an array of images copied from the file embedded images.
/*!
\return Array of TagInfo_Image
\param info A TagInfo_Info object
\param image_count The return location for the length of the returned array.
*/
TagInfo_Image ** taginfo_info_get_images(const TagInfo_Info *info,
                             int *image_count) {
    const Info *i = reinterpret_cast<const Info *>(info);
    ImageList image_list = i->get_images();
    image_list.setAutoDelete(false);
    TagInfo_Image ** images = reinterpret_cast<TagInfo_Image **> (image_list_to_image_array(image_list, *image_count));
    return images;
}


ImageList image_array_to_image_list(Image ** image_array, int image_count) {
    ImageList list;
    list.setAutoDelete(false);
    for(int i = 0; i < image_count; i++) {
        list.append(reinterpret_cast<Image *>(image_array[i]));
    }
    return list;
}

//! Set an array of TagInfo_Image to be embedded into the media file.
/*!
\param info A TagInfo_Info object
\param images An array of TagInfo_Image
\param image_count The length of the TagInfo_Image array
*/
void taginfo_info_set_images(TagInfo_Info *info,
                             TagInfo_Image ** images, const int image_count) {
    Info *i = reinterpret_cast<Info *>(info);
    const ImageList img_list = image_array_to_image_list(reinterpret_cast<Image **>(images), image_count);
    i->set_images(img_list);
}

//! Constructor function for new TagInfo_Image
//! \return Newly created TagInfo_Image 
TagInfo_Image * taginfo_image_new() {
    Image * i = new Image();
    return reinterpret_cast<TagInfo_Image *>(i);
}

//! Get the binary data of the image.
/*!
\param img A TagInfo_Image object
\param bin_data_length The return location for the length of the returned array.
\return Array of bytes
*/
char * taginfo_image_get_data(const TagInfo_Image * img, unsigned int * bin_data_length) {
    if(!img)
        return NULL;
    const Image *i = reinterpret_cast<const Image *>(img);
    return i->get_data(*bin_data_length);
}

//! Set the binary data of the image.
/*!
\param img A TagInfo_Image object
\param bin_data Array of bytes
\param bin_data_length The length of the data array.
*/
void taginfo_image_set_data(TagInfo_Image * img, char * bin_data, unsigned int bin_data_length) {
    if(!img || bin_data_length <= 0)
        return;
    Image *i = reinterpret_cast<Image *>(img);
    i->set_data(bin_data, bin_data_length);
}

//! Get the content's type of the image.
/*!
\param img A TagInfo_Image object
\return image type as TagInfo_ImageContentType
*/
TagInfo_ImageContentType taginfo_image_get_content_type(const TagInfo_Image * img) {
    if(!img)
        return TAG_INFO_IMAGE_CONTENT_OTHER;
    const Image *i = reinterpret_cast<const Image *>(img);
    return static_cast<TagInfo_ImageContentType>(i->get_content_type());
}

//! Set the content's type of the image.
/*!
\param img A TagInfo_Image object
\param itype image type as TagInfo_ImageContentType
*/
void taginfo_image_set_content_type(TagInfo_Image * img, TagInfo_ImageContentType itype) {
    if(!img)
        return;
    Image *i = reinterpret_cast<Image *>(img);
    i->set_content_type(static_cast<Image::ContentType>(itype));
}

//! Get the file's type of the image.
/*!
\param img A TagInfo_Image object
\return image type as TagInfo_ImageFileType
*/
TagInfo_ImageFileType taginfo_image_get_file_type(const TagInfo_Image * img) {
    if(!img)
        return TAG_INFO_IMAGE_FILE_TYPE_UNKNOWN;
    const Image *i = reinterpret_cast<const Image *>(img);
    return static_cast<TagInfo_ImageFileType>(i->get_file_type());
}

//! Set the file's type of the image.
/*!
\param img A TagInfo_Image object
\param image_f_type image type as TagInfo_ImageFileType
*/
void taginfo_image_set_file_type(TagInfo_Image * img, TagInfo_ImageFileType image_f_type) {
    if(!img)
        return;
    Image *i = reinterpret_cast<Image *>(img);
    i->set_file_type(static_cast<Image::FileType>(image_f_type));
}

//! Get the file's type of the image.
/*!
\param img A TagInfo_Image object
\return description as char*
*/
char * taginfo_image_get_description(const TagInfo_Image * img) {
    if(!img)
        return NULL;
    const Image *i = reinterpret_cast<const Image *>(img);
    return ::strdup(i->get_description().toCString(false));
}

//! Set the file's type of the image.
/*!
\param img A TagInfo_Image object
\param new_description as const char*. Will be copied internally
*/
void taginfo_image_set_description(TagInfo_Image * img, const char * new_description) {
    if(!img)
        return;
    Image *i = reinterpret_cast<Image *>(img);
    String s = String();
    if(!new_description)
        i->set_description(s);
    else
        i->set_description(new_description);
}


/*!
 * Frees a TagInfo_Image object.
 */
//!\param img A TagInfo_Image object
void taginfo_image_free(TagInfo_Image * img) {
    if(!img)
        return;
    Image *i = reinterpret_cast<Image *>(img);
    delete i;
    i = NULL;
}

/*!
 * Frees an array of TagInfo_Image as returned from taginfo_info_get_images()..
 */
//!\param img_arr An array of TagInfo_Image
void taginfo_image_array_free(TagInfo_Image ** img_arr) {
    if(!img_arr)
        return;
    for(TagInfo_Image ** it = img_arr; *it != NULL; it++) {
        taginfo_image_free(*it);
    }
    img_arr = NULL;
}
//*****************/IMAGES***************************************************************


//*****************LYRICS****************************************************************
//! Get a lyrics string from the media file.
/*!
\return Lyrics string
*/
//! \param info A TagInfo_Info object
char * taginfo_info_get_lyrics(const TagInfo_Info *info) {
    const Info *i = reinterpret_cast<const Info *>(info);
    String lyrics = i->get_lyrics();
    char *s = ::strdup(lyrics.toCString(true));
    if(stringManagementEnabled && s)
        strings.prepend(s);
    return s;
}

//! Set a lyrics string to the media file.
/*!
\param lyrics Lyrics string
*/
//! \param info A TagInfo_Info object
void taginfo_info_set_lyrics(TagInfo_Info *info, const char * lyrics) {
    Info *i = reinterpret_cast<Info *>(info);
    i->set_lyrics(String(lyrics));
}
//*****************/LYRICS***************************************************************


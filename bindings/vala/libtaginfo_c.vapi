/* taginfo_c.vapi
 *
 * Copyright (C) 2012 - 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 */

[CCode (cprefix = "TagInfo_", lower_case_cprefix = "taginfo_", cheader_filename = "taginfo_c.h")]
namespace TagInfo {
	
	[CCode (cname = "TagInfo_MediaFileType", cprefix = "TAG_INFO_FILE_TYPE_")]
	public enum MediaFileType {
		UNKNOWN,
		AAC,
		AIF,
		APE,
		ASF,
		FLAC,
		M4A,
		M4B,
		M4P,
		MP3,
		MP4,
		MPC,
		OGA,
		OGG,
		TTA,
		WAV,
		WMA,
		WV,
		SPEEX,
		WMV,
		MOD,
		IT,
		XM,
		S3M,
		OPUS
	}
	
	[Compact]
	[CCode (free_function = "taginfo_image_free", cprefix = "taginfo_image_")]
	public class Image {
		
		public Image();
		
		[CCode (cname = "TagInfo_ContentType", cprefix = "TAG_INFO_IMAGE_CONTENT_")]
		public enum ContentType {
			OTHER,
			FILE_ICON,
			OTHER_FILE_ICON,
			COVER_FRONT,
			COVER_BACK,
			LEAFLET_PAGE,
			MEDIA,
			LEAD_ARTIST,
			ARTIST,
			CONDUCTOR,
			BAND,
			COMPOSER,
			LYRICIST,
			RECORDING_LOCATION,
			DURING_RECORDING,
			DURING_PERFORMANCE,
			MOVIESCREENCAPTURE,
			COLOURED_FISH,
			ILLUSTRATION,
			ARTIST_LOGO,
			PUBLISHER_LOGO
		}
		
		[CCode (cname = "TagInfo_ImageFileType", cprefix = "TAG_INFO_IMAGE_FILE_TYPE_")]
		public enum FileType {
			UNKNOWN,
			JPEG,
			PNG,
			GIF,
			BMP
		}
		
		[CCode (array_length_cname = "bin_data_length", array_length_type = "unsigned int")]
		public uint8[] get_data();
		public void set_data([CCode (array_length_cname = "bin_data_length", array_length_type = "unsigned int")] uint8[] data);
		
		public ContentType      get_content_type();
		public void             set_content_type(ContentType content_type);
		
		public FileType         get_file_type();
		public void             set_file_type(FileType file_type);
		
		public string           get_description();
		public void             set_description(string new_description);
	}
	
	
	[Compact]
	[CCode (free_function = "taginfo_info_free")]
	protected class Info {
		//creation method using filename extension
		public static Info create (string filename);
		
		//creation method for known formats/mimetypes
		public static Info create_with_file_type (string filename, MediaFileType media_file_type);
		
		//creation method using mimetype
		public static Info create_from_mime (string filename, string mimetype);
		
		
		public bool is_valid();
		
		// Info <-> File mappings
		public bool load ();
		public bool save ();
		
		
		// TAG ACCESS
		public string album {
			[CCode (cname = "taginfo_info_get_album")]
			owned get;
			[CCode (cname = "taginfo_info_set_album")]
			set;
		}
		public string albumartist {
			[CCode (cname = "taginfo_info_get_albumartist")]
			owned get;
			[CCode (cname = "taginfo_info_set_albumartist")]
			set;
		}
		public string artist {
			[CCode (cname = "taginfo_info_get_artist")]
			owned get;
			[CCode (cname = "taginfo_info_set_artist")]
			set;
		}
		public int beats_per_minute {
			[CCode (cname = "taginfo_info_get_beats_per_minute")]
			get;
			[CCode (cname = "taginfo_info_set_beats_per_minute")]
			set;
		}
		public string comment {
			[CCode (cname = "taginfo_info_get_comment")]
			owned get;
			[CCode (cname = "taginfo_info_set_comment")]
			set;
		}
		public string composer {
			[CCode (cname = "taginfo_info_get_composer")]
			owned get;
			[CCode (cname = "taginfo_info_set_composer")]
			set;
		}
		public string copyright {
			[CCode (cname = "taginfo_info_get_copyright")]
			owned get;
			[CCode (cname = "taginfo_info_set_copyright")]
			set;
		}
		public string encoder {
			[CCode (cname = "taginfo_info_get_encoder")]
			owned get;
			[CCode (cname = "taginfo_info_set_encoder")]
			set;
		}
		public string genre {
			[CCode (cname = "taginfo_info_get_genre")]
			owned get;
			[CCode (cname = "taginfo_info_set_genre")]
			set;
		}
		public bool has_image {
			// A quick lookup without extracting anything
			[CCode (cname = "taginfo_info_get_has_image")]
			get;
		}
		public bool is_compilation {
			[CCode (cname = "taginfo_info_get_is_compilation")]
			get;
			[CCode (cname = "taginfo_info_set_is_compilation")]
			set;
		}
		public string homepage {
			[CCode (cname = "taginfo_info_get_homepage")]
			owned get;
			[CCode (cname = "taginfo_info_set_homepage")]
			set;
		}
		public string original_artist {
			[CCode (cname = "taginfo_info_get_original_artist")]
			owned get;
			[CCode (cname = "taginfo_info_set_original_artist")]
			set;
		}
		public int playcount {
			[CCode (cname = "taginfo_info_get_playcount")]
			get;
			[CCode (cname = "taginfo_info_set_playcount")]
			set;
		}
		public int rating {
			[CCode (cname = "taginfo_info_get_rating")]
			get;
			[CCode (cname = "taginfo_info_set_rating")]
			set;
		}
		public string title {
			[CCode (cname = "taginfo_info_get_title")]
			owned get;
			[CCode (cname = "taginfo_info_set_title")]
			set;
		}
		public int track_count {
			[CCode (cname = "taginfo_info_get_track_count")]
			get;
			[CCode (cname = "taginfo_info_set_track_count")]
			set;
		}
		public int track_number {
			[CCode (cname = "taginfo_info_get_track_number")]
			get;
			[CCode (cname = "taginfo_info_set_track_number")]
			set;
		}
		public int volume_count {
			[CCode (cname = "taginfo_info_get_volume_count")]
			get;
			[CCode (cname = "taginfo_info_set_volume_count")]
			set;
		}
		public int volume_number {
			[CCode (cname = "taginfo_info_get_volume_number")]
			get;
			[CCode (cname = "taginfo_info_set_volume_number")]
			set;
		}
		public int year {
			[CCode (cname = "taginfo_info_get_year")]
			get;
			[CCode (cname = "taginfo_info_set_year")]
			set;
		}
		
		//PROPERTIES
		public int length {
			[CCode (cname = "taginfo_info_get_length")]
			get;
		}
		public int bitrate {
			[CCode (cname = "taginfo_info_get_bitrate")]
			get;
		}
		public int samplerate {
			[CCode (cname = "taginfo_info_get_samplerate")]
			get;
		}
		public int channels {
			[CCode (cname = "taginfo_info_get_channels")]
			get;
		}
		
		// USER LABELS
		public string[] get_artist_labels ();
		public void	 set_artist_labels (string[] labels);
		
		public string[]     get_album_labels ();
		public void         set_album_labels (string[] labels);
		
		public string[]     get_track_labels ();
		public void         set_track_labels (string[] labels);
		
		// IMAGE ACCESS
		public Image[]      get_images ();
		public void         set_images (Image[] images);
		
		// LYRICS ACCESS
		public string lyrics {
			[CCode (cname = "taginfo_info_get_lyrics")]
			owned get;
			[CCode (cname = "taginfo_info_set_lyrics")]
			set;
		}
	}
}


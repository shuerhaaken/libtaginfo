#include "taginfo.h"
#include <stdio.h>
#include <iostream>


using namespace TagInfo;

int main( void ) {
    InfoRef iref("path/to/file.mp3");
    if(iref.is_valid()) {
        if(iref.load()) {
            cout << "Title: " << info->get_title() << endl;
        }
    }
}

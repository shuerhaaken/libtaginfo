using TagInfo;

void main() {
    Info info = Info.factory_make("/path/to/file.ogg");
    info.load();
    string tit = info.title;
    print("title: %s\n", tit);
    print("album: %s\n", info.album);
    print("artist: %s\n", info.artist);
    print("albumartist: %s\n", info.albumartist);
    return;
}

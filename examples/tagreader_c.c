#include <stdio.h>
#include <taginfo_c.h>

int main(void) {
    TagInfo_Info *info;

    info = taginfo_info_create("./path/to/file.mp3");

    if(info == NULL)
      return 1;

    if(taginfo_info_load(info)) {
      char* name = taginfo_info_get_title(info);
      printf("-- TAG --%s\n", name);
      free(name);
    }
    else {
      printf("-- ERROR --\n");
    }

    taginfo_info_free(info);
    return 0;
}

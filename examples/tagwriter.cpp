#include "taginfo.h"
#include <stdio.h>
#include <iostream>

#define TESTNAME "xyz 1 23"

using namespace TagInfo;

int main( void ) {
    Info * info;
    
    //WRITING
    info = Info::create("path/to/file.mp3");
    
    if( info ) {
        info->set_title(TESTNAME);
        info->save();
        delete info;
    }
    
    //READING
    info = Info::create(target);
    if( info ) {
        if( info->load() ) {
            TagLib::String title = info->get_title();
            printf("title: %s\n", title.toCString(false));
        }
        delete info;
    }
}

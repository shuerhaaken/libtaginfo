using TagInfo;


void main() {
    Info info = Info.create("path/to/file.mp3");
    if(!info.load()) {
        print("error load\n");
        return;
    }
    Gdk.Pixbuf pixbuf = new Gdk.Pixbuf.from_file("path/to/image.jpeg");
    assert(pixbuf != null);
    Image[] ia = {};
    try {
        ia += new Image();
        uint8[] data;
        pixbuf.save_to_buffer(out data, "jpeg");
        ia[0].set_data(data);
        ia[0].set_content_type(Image.ContentType.COVER_FRONT);
    }
    catch(Error e) {
        print("%s\n", e.message);
    }
    info.set_images(ia);
    info.save();
}

/*
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */



#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_apetags.h"


#include <apefile.h>


using namespace TagInfo;
using namespace TagLib;



ApeInfo::ApeInfo(const String &filename) : ApeTagInfo(filename) {
    if(file_name.isEmpty()) {
        valid = false;
        printf("File name empty!\n");
    }
    else {
        taglib_file = new APE::File(file_name.toCString(false), true, AudioProperties::Fast);
    }
    if(taglib_file) {
        if(!taglib_file->isOpen()) {
            cout << "Cannot open file '" << file_name << "'" << endl;
            valid = false;
        }
        taglib_apetag = ((APE::File *) taglib_file)->APETag(true);
        if(!taglib_apetag || taglib_apetag->isEmpty()) { // Use fallback
            taglib_tag = ((APE::File *) taglib_file)->tag();
            if(!taglib_tag) {
                printf("Cant get tag object from '%s'\n", file_name.toCString(false));
                valid = false;
            }
        }
    }
    else {
        printf("Cant get tag from '%s'\n", file_name.toCString(false));
        taglib_apetag = NULL;
        valid = false;
    }
}


ApeInfo::~ApeInfo() {
}


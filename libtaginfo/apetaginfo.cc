/*
 * Copyright (C) 2013 -2014 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */



#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_apetags.h"


#include <apefile.h>
#include <apetag.h>


#ifndef DOXYGEN_SHOULD_SKIP_THIS

#define APE_TAG_DISK                "DISC"
#define APE_TAG_DISKNO              "DISCNUMBER"
#define APE_TAG_COMPILATION         "COMPILATION"
#define APE_TAG_ALBUMARTIST         "ALBUM ARTIST" 
#define APE_TAG_ORIGINALARTIST      "ORIGINAL ARTIST" 
#define APE_TAG_TRACK               "TRACK"
#define APE_TAG_COMPOSER            "COMPOSER" 
#define APE_TAG_COMMENT             "COMMENT" 
#define APE_TAG_YEAR                "YEAR" 
#define APE_TAG_LYRICS              "LYRICS" 
#define APE_TAG_LYRICS_UNSYNCED     "UNSYNCED LYRICS"
#define APE_TAG_RATING              "RATING"
#define APE_TAG_PLAYCNT             "PLAY_COUNTER"
#define APE_TAG_COPYRIGHT           "COPYRIGHT"
#define APE_TAG_ENCODER             "ENCODED BY"
#define APE_TAG_HOMEPAGE            "RELATED"
#define APE_TAG_BPM                 "BPM"

#define TRACK_LABELS                "TRACK_LABELS"
#define ALBUM_LABELS                "ALBUM_LABELS"
#define ARTIST_LABELS               "ARTIST_LABELS"

#define APE_TAG_COVER_ART_OTHER             "COVER ART (OTHER)"
#define APE_TAG_COVER_ART_ICON              "COVER ART (ICON)"
#define APE_TAG_COVER_ART_OTHER_ICON        "COVER ART (OTHER ICON)"
#define APE_TAG_COVER_ART_FRONT             "COVER ART (FRONT)"
#define APE_TAG_COVER_ART_BACK              "COVER ART (BACK)"
#define APE_TAG_COVER_ART_LEAFLET           "COVER ART (LEAFLET)"
#define APE_TAG_COVER_ART_MEDIA             "COVER ART (MEDIA)"
#define APE_TAG_COVER_ART_LEAD              "COVER ART (LEAD)"
#define APE_TAG_COVER_ART_ARTIST            "COVER ART (ARTIST)"
#define APE_TAG_COVER_ART_CONDUCTOR         "COVER ART (CONDUCTOR)"
#define APE_TAG_COVER_ART_BAND              "COVER ART (BAND)"
#define APE_TAG_COVER_ART_COMPOSER          "COVER ART (COMPOSER)"
#define APE_TAG_COVER_ART_LYRICIST          "COVER ART (LYRICIST)"
#define APE_TAG_COVER_ART_STUDIO            "COVER ART (STUDIO)"
#define APE_TAG_COVER_ART_RECORDING         "COVER ART (RECORDING)"
#define APE_TAG_COVER_ART_PERFORMANCE       "COVER ART (PERFORMANCE)"
#define APE_TAG_COVER_ART_MOVIE_SCENE       "COVER ART (MOVIE SCENE)"
#define APE_TAG_COVER_ART_COLORED_FISH      "COVER ART (COLORED FISH)"
#define APE_TAG_COVER_ART_ILLUSTRATION      "COVER ART (ILLUSTRATION)"
#define APE_TAG_COVER_ART_BAND_LOGO         "COVER ART (BAND LOGO)"
#define APE_TAG_COVER_ART_PUBLISHER_LOGO    "COVER ART (PUBLISHER LOGO)"

#endif

using namespace TagInfo;
using namespace TagLib;



void set_item(APE::Tag * apetag, const char * key, const String &value, bool replace = true) {
    apetag->removeItem(key);
    if(!value.isEmpty())
        apetag->addValue(key, value, replace);
}

void extract_image(const APE::Item &image_item, Image *& image) {
    image->set_file_type(Image::TYPE_UNKNOWN);
    if(image_item.type() == APE::Item::Binary) {
        ByteVector CoverData = image_item.binaryData();
        if(CoverData.size() > 0) {
            image->set_data(CoverData);
            return;
        }
    }
}

String image_type_to_ape_image_name(const Image::ContentType& image_type) {
    switch(image_type) {
        case Image::CONTENT_COVER_FRONT :
            return APE_TAG_COVER_ART_FRONT;
        case Image::CONTENT_COVER_BACK :
            return APE_TAG_COVER_ART_BACK;
        case Image::CONTENT_OTHER :
            return APE_TAG_COVER_ART_OTHER;
        case Image::CONTENT_FILE_ICON :
            return APE_TAG_COVER_ART_ICON;
        case Image::CONTENT_OTHER_FILE_ICON :
            return APE_TAG_COVER_ART_OTHER_ICON;
        case Image::CONTENT_LEAFLET_PAGE :
            return APE_TAG_COVER_ART_LEAFLET;
        case Image::CONTENT_MEDIA  :
            return APE_TAG_COVER_ART_MEDIA;
        case Image::CONTENT_LEAD_ARTIST :
            return APE_TAG_COVER_ART_LEAD;
        case Image::CONTENT_ARTIST :
            return APE_TAG_COVER_ART_ARTIST;
        case Image::CONTENT_CONDUCTOR :
            return APE_TAG_COVER_ART_CONDUCTOR;
        case Image::CONTENT_BAND :
            return APE_TAG_COVER_ART_BAND;
        case Image::CONTENT_COMPOSER :
            return APE_TAG_COVER_ART_COMPOSER;
        case Image::CONTENT_LYRICIST :
            return APE_TAG_COVER_ART_LYRICIST;
        case Image::CONTENT_RECORDING_LOCATION :
            return APE_TAG_COVER_ART_STUDIO;
        case Image::CONTENT_DURING_RECORDING :
            return APE_TAG_COVER_ART_RECORDING;
        case Image::CONTENT_DURING_PERFORMANCE :
            return APE_TAG_COVER_ART_PERFORMANCE;;
        case Image::CONTENT_MOVIESCREENCAPTURE :
            return APE_TAG_COVER_ART_MOVIE_SCENE;;
        case Image::CONTENT_COLOURED_FISH :
            return APE_TAG_COVER_ART_COLORED_FISH;
        case Image::CONTENT_ILLUSTRATION :
            return APE_TAG_COVER_ART_ILLUSTRATION;
        case Image::CONTENT_ARTIST_LOGO :
            return APE_TAG_COVER_ART_BAND_LOGO;
        case Image::CONTENT_PUBLISHER_LOGO :
            return APE_TAG_COVER_ART_PUBLISHER_LOGO;
        default:
            return APE_TAG_COVER_ART_OTHER;
    }
}

Image::ContentType ape_image_type_to_image_type(const String &nme) {
    if(nme == APE_TAG_COVER_ART_FRONT)
        return Image::CONTENT_COVER_FRONT;
    else if(nme == APE_TAG_COVER_ART_OTHER)
        return Image::CONTENT_OTHER;
    else if(nme == APE_TAG_COVER_ART_BACK)
        return Image::CONTENT_COVER_BACK;
    else if(nme == APE_TAG_COVER_ART_ICON)
        return Image::CONTENT_FILE_ICON;
    else if(nme == APE_TAG_COVER_ART_OTHER_ICON)
        return Image::CONTENT_OTHER_FILE_ICON;
    else if(nme == APE_TAG_COVER_ART_LEAFLET)
        return Image::CONTENT_LEAFLET_PAGE;
    else if(nme == APE_TAG_COVER_ART_MEDIA)
        return Image::CONTENT_MEDIA;
    else if(nme == APE_TAG_COVER_ART_LEAD)
        return Image::CONTENT_LEAD_ARTIST;
    else if(nme == APE_TAG_COVER_ART_ARTIST)
        return Image::CONTENT_ARTIST;
    else if(nme == APE_TAG_COVER_ART_CONDUCTOR)
        return Image::CONTENT_CONDUCTOR;
    else if(nme == APE_TAG_COVER_ART_BAND)
        return Image::CONTENT_BAND;
    else if(nme == APE_TAG_COVER_ART_COMPOSER)
        return Image::CONTENT_COMPOSER;
    else if(nme == APE_TAG_COVER_ART_LYRICIST)
        return Image::CONTENT_LYRICIST;
    else if(nme == APE_TAG_COVER_ART_STUDIO)
        return Image::CONTENT_RECORDING_LOCATION;
    else if(nme == APE_TAG_COVER_ART_RECORDING)
        return Image::CONTENT_DURING_RECORDING;
    else if(nme == APE_TAG_COVER_ART_PERFORMANCE)
        return Image::CONTENT_DURING_PERFORMANCE;
    else if(nme == APE_TAG_COVER_ART_MOVIE_SCENE)
        return Image::CONTENT_MOVIESCREENCAPTURE;
    else if(nme == APE_TAG_COVER_ART_COLORED_FISH)
        return Image::CONTENT_COLOURED_FISH;
    else if(nme == APE_TAG_COVER_ART_ILLUSTRATION)
        return Image::CONTENT_ILLUSTRATION;
    else if(nme == APE_TAG_COVER_ART_BAND_LOGO)
        return Image::CONTENT_ARTIST_LOGO;
    else if(nme == APE_TAG_COVER_ART_PUBLISHER_LOGO)
        return Image::CONTENT_PUBLISHER_LOGO;
    else
        return Image::CONTENT_OTHER;
}


ApeTagInfo::ApeTagInfo(const String &filename) : Info(filename) {
    taglib_apetag = NULL;
}


ApeTagInfo::~ApeTagInfo() {
}


bool ApeTagInfo::load(void) {
    if(Info::load()) {
        if(taglib_apetag && !taglib_apetag->isEmpty()) {
            if(!taglib_tag)
                load_base_tags((Tag *)taglib_apetag);
            if(taglib_apetag->itemListMap().contains(APE_TAG_COMPOSER)) {
                composer = taglib_apetag->itemListMap()[APE_TAG_COMPOSER].toString();
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_COPYRIGHT)) {
                copyright = taglib_apetag->itemListMap()[APE_TAG_COPYRIGHT].toString();
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_ORIGINALARTIST)) {
                original_artist = taglib_apetag->itemListMap()[APE_TAG_ORIGINALARTIST].toString();
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_ENCODER)) {
                encoder = taglib_apetag->itemListMap()[APE_TAG_ENCODER].toString();
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_HOMEPAGE)) {
                homepage = taglib_apetag->itemListMap()[APE_TAG_HOMEPAGE].toString();
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_TRACK) && !taglib_apetag->itemListMap()[APE_TAG_TRACK].isEmpty()) {
                StringList list = taglib_apetag->itemListMap()[APE_TAG_TRACK].toString().split("/");
                if(list.size() == 2) {
                    track_number = list.front().toInt();
                    track_count  = list.back().toInt();
                }
                else if(list.size() == 1) {
                    track_number = list.front().toInt();
                }
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_DISK) && !taglib_apetag->itemListMap()[APE_TAG_DISK].isEmpty()) {
                StringList list = taglib_apetag->itemListMap()[APE_TAG_DISK].toString().split("/");
                if(list.size() == 2) {
                    volume_number = list.front().toInt();
                    volume_count  = list.back().toInt();
                }
                else if(list.size() == 1) {
                    track_number = list.front().toInt();
                }
            }
            else if(taglib_apetag->itemListMap().contains(APE_TAG_DISKNO)) {
                volume_number = taglib_apetag->itemListMap()[APE_TAG_DISKNO].toString().toInt();
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_BPM)) {
                beats_per_minute = taglib_apetag->itemListMap()[APE_TAG_BPM].toString().toInt();
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_COMPILATION)) {
                is_compilation = 
                    taglib_apetag->itemListMap()[APE_TAG_COMPILATION].toString() == String("1");
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_ALBUMARTIST)) {
                album_artist = taglib_apetag->itemListMap()[APE_TAG_ALBUMARTIST].toString();//toStringList().front();
            }
            else if(taglib_apetag->itemListMap().contains("ALBUMARTIST")) {
                album_artist = taglib_apetag->itemListMap()["ALBUMARTIST"].toString();//toStringList().front();
            }
            
            // Rating 
            if(taglib_apetag->itemListMap().contains(APE_TAG_RATING)) {
                int rat = 0;
                rat = taglib_apetag->itemListMap()[APE_TAG_RATING].toString().toInt();
                if(rat) {
                    if(rat > 5)
                        rating = popularity_to_rating(rat);
                    else
                        rating = rat;
                }
            }
            if(taglib_apetag->itemListMap().contains(APE_TAG_PLAYCNT)) {
                playcount = taglib_apetag->itemListMap()[APE_TAG_PLAYCNT].toString().toInt();
                if(playcount < 0)
                    playcount = 0;
            }
            // Labels
            if(track_labels.size() == 0) {
                if(taglib_apetag->itemListMap().contains(TRACK_LABELS)) {
                    track_labels_string = taglib_apetag->itemListMap()[TRACK_LABELS].toString();
                    track_labels = split(track_labels_string, "|");
                }
            }
            if(artist_labels.size() == 0) {
                if(taglib_apetag->itemListMap().contains(ARTIST_LABELS)) {
                    artist_labels_string = taglib_apetag->itemListMap()[ARTIST_LABELS].toString();
                    artist_labels = split(artist_labels_string, "|");
                }
            }
            if(album_labels.size() == 0) {
                if(taglib_apetag->itemListMap().contains(ALBUM_LABELS)) {
                    album_labels_string = taglib_apetag->itemListMap()[ALBUM_LABELS].toString();
                    album_labels = split(album_labels_string, "|");
                }
            }
            APE::ItemListMap map = taglib_apetag->itemListMap();
            for(APE::ItemListMap::ConstIterator it = map.begin(); it != map.end(); ++it) {
                if(it->first == APE_TAG_COVER_ART_OTHER          ||
                   it->first == APE_TAG_COVER_ART_ICON	         ||
                   it->first == APE_TAG_COVER_ART_OTHER_ICON     ||
                   it->first == APE_TAG_COVER_ART_FRONT          ||
                   it->first == APE_TAG_COVER_ART_BACK           ||
                   it->first == APE_TAG_COVER_ART_LEAFLET        ||
                   it->first == APE_TAG_COVER_ART_MEDIA          ||
                   it->first == APE_TAG_COVER_ART_LEAD           ||
                   it->first == APE_TAG_COVER_ART_ARTIST         ||
                   it->first == APE_TAG_COVER_ART_CONDUCTOR      ||
                   it->first == APE_TAG_COVER_ART_BAND           ||
                   it->first == APE_TAG_COVER_ART_COMPOSER       ||
                   it->first == APE_TAG_COVER_ART_LYRICIST       ||
                   it->first == APE_TAG_COVER_ART_STUDIO         ||
                   it->first == APE_TAG_COVER_ART_RECORDING      ||
                   it->first == APE_TAG_COVER_ART_PERFORMANCE    ||
                   it->first == APE_TAG_COVER_ART_MOVIE_SCENE    ||
                   it->first == APE_TAG_COVER_ART_COLORED_FISH   ||
                   it->first == APE_TAG_COVER_ART_ILLUSTRATION   ||
                   it->first == APE_TAG_COVER_ART_BAND_LOGO      ||
                   it->first == APE_TAG_COVER_ART_PUBLISHER_LOGO   ) {
                    has_image = true;
                }
            }
            return true;
        }
    }
    return false;
}


bool ApeTagInfo::save(void) {
    if(taglib_apetag) {
        if(changedflag) {
            if(changedflag & CHANGED_COMPOSER_TAG)
                set_item(taglib_apetag, APE_TAG_COMPOSER, composer, true);
            if(changedflag & CHANGED_COPYRIGHT_TAG)
                set_item(taglib_apetag, APE_TAG_COPYRIGHT, copyright, true);
            if(changedflag & CHANGED_ORIGINALARTIST_TAG)
                set_item(taglib_apetag, APE_TAG_ORIGINALARTIST, original_artist, true);
            if(changedflag & CHANGED_ENCODER_TAG)
                set_item(taglib_apetag, APE_TAG_ENCODER, encoder, true);
            if(changedflag & CHANGED_HOMEPAGE_TAG)
                set_item(taglib_apetag, APE_TAG_HOMEPAGE, homepage, true);
            if(changedflag & CHANGED_TRACK_NUMBER) {
                if(!(changedflag & CHANGED_TRACK_COUNT)) {
                    // prevent overwrite in save_base_tags()
                    changedflag &= ~(CHANGED_TRACK_NUMBER);
                    taglib_apetag->setTrack(track_number);
                }
                else {
                    // prevent overwrite in save_base_tags()
                    changedflag &= ~(CHANGED_TRACK_NUMBER);
                    changedflag &= ~(CHANGED_TRACK_COUNT);
                    String v;
                    v += String::number(track_number);
                    v += String('/');
                    v += String::number(track_count);
                    taglib_apetag->addValue(APE_TAG_TRACK, v, true);
                }
            }
            if(changedflag & CHANGED_TRACK_COUNT) {
                // prevent overwrite in save_base_tags()
                changedflag &= ~(CHANGED_TRACK_NUMBER);
                changedflag &= ~(CHANGED_TRACK_COUNT);
                String v;
                v += String::number(track_number);
                v += String('/');
                v += String::number(track_count);
                taglib_apetag->addValue(APE_TAG_TRACK, v, true);
            }
            if(changedflag & CHANGED_DATA_VOL_NUM) {
                if(!(changedflag & CHANGED_DATA_VOL_CNT)) {
                    // prevent overwrite in save_base_tags()
                    changedflag &= ~(CHANGED_DATA_VOL_NUM);
                    String vn = String::number(volume_number);
                    taglib_apetag->addValue(APE_TAG_DISK, vn, true);
                    taglib_apetag->addValue(APE_TAG_DISKNO, vn, true);
                }
                else {
                    // prevent overwrite in save_base_tags()
                    changedflag &= ~(CHANGED_DATA_VOL_NUM);
                    changedflag &= ~(CHANGED_DATA_VOL_CNT);
                    String v;
                    v += String::number(volume_number);
                    v += String('/');
                    v += String::number(volume_count);
                    taglib_apetag->addValue(APE_TAG_DISK, v, true);
                }
            }
            if(changedflag & CHANGED_BPM_TAG) {
                String vn = String::number(beats_per_minute);
                taglib_apetag->addValue(APE_TAG_BPM, vn, true);
            }
            if(changedflag & CHANGED_DATA_VOL_CNT) {
                // prevent overwrite in save_base_tags()
                changedflag &= ~(CHANGED_DATA_VOL_NUM);
                changedflag &= ~(CHANGED_DATA_VOL_CNT);
                String v;
                v += String::number(volume_number);
                v += String('/');
                v += String::number(volume_count);
                taglib_apetag->addValue(APE_TAG_DISK, v, true);
            }
            
            if(changedflag & CHANGED_IS_COMPILATION_TAG) {
                if(is_compilation) {
                    taglib_apetag->addValue(APE_TAG_COMPILATION, "1", true);
                }
                else {
                    taglib_apetag->addValue(APE_TAG_COMPILATION, "0", true);
                }
            }
            if(changedflag & CHANGED_DATA_ALBUMARTIST)
                set_item(taglib_apetag, APE_TAG_ALBUMARTIST, album_artist, true);
            
            if(changedflag & CHANGED_DATA_RATING)
                taglib_apetag->addValue(APE_TAG_RATING, String::number(rating_to_popularity(rating)), true);
            
            if(changedflag & CHANGED_DATA_PLAYCOUNT)
                taglib_apetag->addValue(APE_TAG_PLAYCNT, String::number(playcount), true);
            
            if(changedflag & CHANGED_TRACK_LABELS)
                set_item(taglib_apetag, TRACK_LABELS, track_labels_string, true);
            if(changedflag & CHANGED_ARTIST_LABELS)
                set_item(taglib_apetag, ARTIST_LABELS, artist_labels_string, true);
            if(changedflag & CHANGED_ALBUM_LABELS)
                set_item(taglib_apetag, ALBUM_LABELS, album_labels_string, true);
            save_base_tags((Tag *)taglib_apetag);
        }
    }
    return Info::save();
}

ImageList ApeTagInfo::get_images() const {
    ImageList images;
    if(taglib_apetag && !taglib_apetag->isEmpty()) {
        if(taglib_apetag) {
            Image * image = NULL;
            APE::ItemListMap map = taglib_apetag->itemListMap();
            for(APE::ItemListMap::ConstIterator it = map.begin(); it != map.end(); ++it) {
                if(it->first == APE_TAG_COVER_ART_OTHER          ||
                   it->first == APE_TAG_COVER_ART_ICON	         ||
                   it->first == APE_TAG_COVER_ART_OTHER_ICON     ||
                   it->first == APE_TAG_COVER_ART_FRONT          ||
                   it->first == APE_TAG_COVER_ART_BACK           ||
                   it->first == APE_TAG_COVER_ART_LEAFLET        ||
                   it->first == APE_TAG_COVER_ART_MEDIA          ||
                   it->first == APE_TAG_COVER_ART_LEAD           ||
                   it->first == APE_TAG_COVER_ART_ARTIST         ||
                   it->first == APE_TAG_COVER_ART_CONDUCTOR      ||
                   it->first == APE_TAG_COVER_ART_BAND           ||
                   it->first == APE_TAG_COVER_ART_COMPOSER       ||
                   it->first == APE_TAG_COVER_ART_LYRICIST       ||
                   it->first == APE_TAG_COVER_ART_STUDIO         ||
                   it->first == APE_TAG_COVER_ART_RECORDING      ||
                   it->first == APE_TAG_COVER_ART_PERFORMANCE    ||
                   it->first == APE_TAG_COVER_ART_MOVIE_SCENE    ||
                   it->first == APE_TAG_COVER_ART_COLORED_FISH   ||
                   it->first == APE_TAG_COVER_ART_ILLUSTRATION   ||
                   it->first == APE_TAG_COVER_ART_BAND_LOGO      ||
                   it->first == APE_TAG_COVER_ART_PUBLISHER_LOGO   ) {
                    image = new Image();
                    extract_image(it->second, image);
                    if(!image->get_data().isEmpty()) {
                        String apetype = String(it->first);
                        image->set_content_type(ape_image_type_to_image_type(apetype));
                        images.prepend(image);
                    }
                    else {
                        delete image;
                    }
               }
            }
        }
        
    }
    return images;
}

void ApeTagInfo::set_images(const ImageList images) {
    if(taglib_apetag) {
        taglib_apetag->removeItem(APE_TAG_COVER_ART_OTHER);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_ICON);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_OTHER_ICON);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_FRONT);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_BACK);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_LEAFLET);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_MEDIA);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_LEAD);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_ARTIST);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_CONDUCTOR);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_BAND);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_COMPOSER);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_LYRICIST);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_STUDIO);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_RECORDING);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_PERFORMANCE);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_MOVIE_SCENE);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_COLORED_FISH);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_ILLUSTRATION);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_BAND_LOGO);
        taglib_apetag->removeItem(APE_TAG_COVER_ART_PUBLISHER_LOGO);
        
        for(ImageList::ConstIterator it = images.begin(); it != images.end(); ++it) {
            if((*it)->get_data().isEmpty())
                continue;
            ByteVector image_vect((*it)->get_data());
            
            APE::Item imageItem = APE::Item();
            imageItem.setType(APE::Item::Binary);
            String key = image_type_to_ape_image_name((*it)->get_content_type());
            imageItem.setKey(key);
            imageItem.setBinaryData(image_vect);
            taglib_apetag->setItem(key, imageItem);
        }
    }
}


String ApeTagInfo::get_lyrics() const {
    if(taglib_apetag->itemListMap().contains(APE_TAG_LYRICS))
        return taglib_apetag->itemListMap()[APE_TAG_LYRICS].toString();
    return String();
}


void ApeTagInfo::set_lyrics(const String &lyrics) {
    if(taglib_apetag) {
        taglib_apetag->removeItem(APE_TAG_LYRICS);
        if(!lyrics.isEmpty())
            taglib_apetag->addValue(APE_TAG_LYRICS, lyrics);
        return;
    }
}


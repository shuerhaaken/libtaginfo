/*
 * Copyright (C) 2008-2013 J.Rios <anonbeat@gmail.com>
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Matias De lellis <mati86dl@gmail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */




#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_asftags.h"


#include <asffile.h>
#include <asftag.h>
#include <asfpicture.h>


#define COMPILATION_FLAG         "WM/PartOfACompilation"
#define PICTURE_FLAG             "WM/Picture"
#define TOTAL_TRACKS             "TrackTotal"
#define ALBUM_ARTIST             "WM/AlbumArtist"
#define VOLUME_NUMBER            "WM/PartOfSet"
#define COMPOSER                 "WM/Composer"
#define ORIGINAL_ARTIST          "WM/OriginalArtist"
#define COPYRIGHT                "Copyright"
#define ENCODER                  "WM/EncodedBy"
#define HOMEPAGE                 "URL"
#define SHARED_RATING            "WM/SharedUserRating"
#define TRACK_LABELS             "TRACK_LABELS"
#define ALBUM_LABELS             "ALBUM_LABELS"
#define ARTIST_LABELS            "ARTIST_LABELS"
#define LYRICS                   "WM/Lyrics"
#define BPM_TAG                  "WM/BeatsPerMinute"


using namespace TagInfo;



inline int wm_rating_to_rating(const int rating) {
    if(rating <= 0)
        return 0;
    if(rating < 25)
        return 1;
    if(rating < 50)
        return 2;
    if(rating < 75)
        return 3;
    if(rating < 99)
        return 4;
    return 5;
}

Image::ContentType get_content_type_from_pic_type(const ASF::Picture::Type& tpe) {
    switch(tpe) {
        case ASF::Picture::FrontCover :
            return Image::CONTENT_COVER_FRONT;
        case ASF::Picture::BackCover :
            return Image::CONTENT_COVER_BACK;
        case ASF::Picture::Other :
            return Image::CONTENT_OTHER;
        case ASF::Picture::FileIcon :
            return Image::CONTENT_FILE_ICON;
        case ASF::Picture::OtherFileIcon :
            return Image::CONTENT_OTHER_FILE_ICON;
        case ASF::Picture::LeafletPage :
            return Image::CONTENT_LEAFLET_PAGE;
        case ASF::Picture::Media :
            return Image::CONTENT_MEDIA;
        case ASF::Picture::LeadArtist :
            return Image::CONTENT_LEAD_ARTIST;
        case ASF::Picture::Artist :
            return Image::CONTENT_ARTIST;
        case ASF::Picture::Conductor :
            return Image::CONTENT_CONDUCTOR;
        case ASF::Picture::Band :
            return Image::CONTENT_BAND;
        case ASF::Picture::Composer :
            return Image::CONTENT_COMPOSER;
        case ASF::Picture::Lyricist :
            return Image::CONTENT_LYRICIST;
        case ASF::Picture::RecordingLocation :
            return Image::CONTENT_RECORDING_LOCATION;
        case ASF::Picture::DuringRecording :
            return Image::CONTENT_DURING_RECORDING;
        case ASF::Picture::DuringPerformance :
            return Image::CONTENT_DURING_PERFORMANCE;
        case ASF::Picture::MovieScreenCapture :
            return Image::CONTENT_MOVIESCREENCAPTURE;
        case ASF::Picture::ColouredFish :
            return Image::CONTENT_COLOURED_FISH;
        case ASF::Picture::Illustration :
            return Image::CONTENT_ILLUSTRATION;
        case ASF::Picture::BandLogo :
            return Image::CONTENT_ARTIST_LOGO;
        case ASF::Picture::PublisherLogo :
            return Image::CONTENT_PUBLISHER_LOGO;
        default:
            return Image::CONTENT_OTHER;
    }
}

ASF::Picture::Type image_type_to_picture_type(const Image::ContentType &image_type) {
    switch(image_type) {
        case Image::CONTENT_COVER_FRONT :
            return ASF::Picture::FrontCover;
        case Image::CONTENT_COVER_BACK :
            return ASF::Picture::BackCover;
        case Image::CONTENT_OTHER :
            return ASF::Picture::Other;
        case Image::CONTENT_FILE_ICON :
            return ASF::Picture::FileIcon;
        case Image::CONTENT_OTHER_FILE_ICON :
            return ASF::Picture::OtherFileIcon;
        case Image::CONTENT_LEAFLET_PAGE :
            return ASF::Picture::LeafletPage;
        case Image::CONTENT_MEDIA  :
            return ASF::Picture::Media;
        case Image::CONTENT_LEAD_ARTIST :
            return ASF::Picture::LeadArtist;
        case Image::CONTENT_ARTIST :
            return ASF::Picture::Artist;
        case Image::CONTENT_CONDUCTOR :
            return ASF::Picture::Conductor;
        case Image::CONTENT_BAND :
            return ASF::Picture::Band;
        case Image::CONTENT_COMPOSER :
            return ASF::Picture::Composer;
        case Image::CONTENT_LYRICIST :
            return ASF::Picture::Lyricist;
        case Image::CONTENT_RECORDING_LOCATION :
            return ASF::Picture::RecordingLocation;
        case Image::CONTENT_DURING_RECORDING :
            return ASF::Picture::DuringRecording;
        case Image::CONTENT_DURING_PERFORMANCE :
            return ASF::Picture::DuringPerformance;
        case Image::CONTENT_MOVIESCREENCAPTURE :
            return ASF::Picture::MovieScreenCapture;
        case Image::CONTENT_COLOURED_FISH :
            return ASF::Picture::ColouredFish;
        case Image::CONTENT_ILLUSTRATION :
            return ASF::Picture::Illustration;
        case Image::CONTENT_ARTIST_LOGO :
            return ASF::Picture::BandLogo;
        case Image::CONTENT_PUBLISHER_LOGO :
            return ASF::Picture::PublisherLogo;
        default:
            return ASF::Picture::Other;
    }
}

AsfInfo::AsfInfo(const String &filename) : Info(filename) {
    if(file_name.isEmpty()) {
        valid = false;
        printf("File name empty!\n");
    }
    else {
        taglib_file = new ASF::File(file_name.toCString(false), true, AudioProperties::Fast);
    }
    if(taglib_file) {
        if(!taglib_file->isOpen()) {
            cout << "Cannot open file '" << file_name << "'" << endl;
            valid = false;
            return;
        }
        asf_tag = ((ASF::File *) taglib_file)->tag();
        if(!asf_tag)
            valid = false;
    }
    else {
        printf("Cant get xiphcomment from '%s'\n", file_name.toCString(false));
        asf_tag = NULL;
        valid = false;
    }
}


AsfInfo::~AsfInfo() {
}


bool AsfInfo::load(void) {
    if(Info::load()) {
        if(asf_tag) {
            load_base_tags((Tag *)asf_tag);
            
            if(asf_tag->attributeListMap().contains(VOLUME_NUMBER) && !asf_tag->attributeListMap()[ VOLUME_NUMBER ].isEmpty()) {
                StringList list = asf_tag->attributeListMap()[ VOLUME_NUMBER ].front().toString().split("/");
                if(list.size() == 2) {
                    volume_number = list.front().toInt();
                    volume_count  = list.back().toInt(); 
                }
                else if(list.size() == 1) {
                    volume_number = list.front().toInt();
                }
            }
            if(asf_tag->attributeListMap().contains(COMPOSER)) {
                composer = asf_tag->attributeListMap()[ COMPOSER ].front().toString();
            }
            if(asf_tag->attributeListMap().contains(ORIGINAL_ARTIST)) {
                original_artist = asf_tag->attributeListMap()[ ORIGINAL_ARTIST ].front().toString();
            }
            if(asf_tag->attributeListMap().contains(TOTAL_TRACKS)) {
                track_count = (int) asf_tag->attributeListMap()[ TOTAL_TRACKS ].front().toUInt();
            }
            if(asf_tag->attributeListMap().contains(BPM_TAG)) {
                beats_per_minute = 
                    (int)asf_tag->attributeListMap()[ BPM_TAG ].front().toUInt();
            }
            if(asf_tag->attributeListMap().contains(COPYRIGHT)) {
                copyright = asf_tag->attributeListMap()[ COPYRIGHT ].front().toString();
            }
            if(asf_tag->attributeListMap().contains(ENCODER)) {
                encoder = asf_tag->attributeListMap()[ ENCODER ].front().toString();
            }
            if(asf_tag->attributeListMap().contains(HOMEPAGE)) {
                homepage = asf_tag->attributeListMap()[ HOMEPAGE ].front().toString();
            }
            if(asf_tag->attributeListMap().contains(COMPILATION_FLAG)) {
                is_compilation =
                    asf_tag->attributeListMap()[COMPILATION_FLAG].front().toString() == String("1");
            }
            if(asf_tag->attributeListMap().contains(ALBUM_ARTIST)) {
                album_artist = asf_tag->attributeListMap()[ ALBUM_ARTIST ].front().toString();
            }
            int rat = 0;
            if(asf_tag->attributeListMap().contains(SHARED_RATING)) {
                rat = (int)asf_tag->attributeListMap()[ SHARED_RATING ].front().toUInt();
            }
            if(!rat && asf_tag->attributeListMap().contains("Rating")) {
                rat = (int)asf_tag->attributeListMap()[ "Rating" ].front().toUInt();
            }
            if(rat) {
                if(rat > 5)
                    rating = wm_rating_to_rating(rat);
                else
                    rating = rat;
            }
            if(track_labels.size() == 0) {
                if(asf_tag->attributeListMap().contains(TRACK_LABELS)) {
                    track_labels_string = asf_tag->attributeListMap()[ TRACK_LABELS ].front().toString();
                    track_labels = split(track_labels_string, "|");
                }
            }
            if(artist_labels.size() == 0) {
                if(asf_tag->attributeListMap().contains(ARTIST_LABELS)) {
                    artist_labels_string = asf_tag->attributeListMap()[ ARTIST_LABELS ].front().toString();
                    artist_labels = split(artist_labels_string, "|");
                }
            }
            if(album_labels.size() == 0) {
                if(asf_tag->attributeListMap().contains(ALBUM_LABELS)) {
                    album_labels_string = asf_tag->attributeListMap()[ ALBUM_LABELS ].front().toString();
                    album_labels = split(album_labels_string, "|");
                }
            }
        }
    }
    else {
        printf("Error: Could not load tags from file '%s'\n", file_name.toCString(true));
        return false; 
    }
    return true;
}


void check_asf_label_frame(ASF::Tag * asftag, const char * description, const String &value) {
    if(asftag->attributeListMap().contains(description))
        asftag->removeItem(description);
    if(!value.isEmpty()) {
            asftag->setAttribute(description, value);
    }
}


bool AsfInfo::save(void) {
    if(asf_tag) {
        if(changedflag) {
            if(changedflag & CHANGED_DATA_VOL_NUM || changedflag & CHANGED_DATA_VOL_CNT) {
                asf_tag->removeItem(VOLUME_NUMBER);
                if(volume_count != 0) {
                    if(volume_number != 0) {
                        String v;
                        v+=String::number(volume_number);
                        v+=String('/');
                        v+=String::number(volume_count);
                        asf_tag->setAttribute(VOLUME_NUMBER, v);
                    }
                    else {
                        asf_tag->setAttribute(VOLUME_NUMBER, String::number(volume_number));
                    }
                }
            }
            if(changedflag & CHANGED_TRACK_COUNT) {
                asf_tag->removeItem(TOTAL_TRACKS);
                asf_tag->setAttribute(TOTAL_TRACKS, (uint)track_count);
            }
            if(changedflag & CHANGED_COMPOSER_TAG) {
                asf_tag->removeItem(COMPOSER);
                asf_tag->setAttribute(COMPOSER, composer);
            }
            if(changedflag & CHANGED_ORIGINALARTIST_TAG) {
                asf_tag->removeItem(ORIGINAL_ARTIST);
                asf_tag->setAttribute(ORIGINAL_ARTIST, original_artist);
            }
            if(changedflag & CHANGED_COPYRIGHT_TAG) {
                asf_tag->removeItem(COPYRIGHT);
                asf_tag->setAttribute(COPYRIGHT, copyright);
            }
            if(changedflag & CHANGED_ENCODER_TAG) {
                asf_tag->removeItem(ENCODER);
                asf_tag->setAttribute(ENCODER, encoder);
            }
            if(changedflag & CHANGED_HOMEPAGE_TAG) {
                asf_tag->removeItem(HOMEPAGE);
                asf_tag->setAttribute(HOMEPAGE, homepage);
            }
            if(changedflag & CHANGED_IS_COMPILATION_TAG) {
                asf_tag->removeItem(COMPILATION_FLAG);
                if(is_compilation)
                    asf_tag->setAttribute(COMPILATION_FLAG, String("1"));
                else
                    asf_tag->setAttribute(COMPILATION_FLAG, String("0"));
            }
            if(changedflag & CHANGED_DATA_ALBUMARTIST) {
                asf_tag->removeItem(ALBUM_ARTIST);
                asf_tag->setAttribute(ALBUM_ARTIST, album_artist);
            }
            if(changedflag & CHANGED_DATA_RATING) {
                asf_tag->removeItem(SHARED_RATING);
                int WMRatings[] = { 0, 0, 1, 25, 50, 75, 99 };
                asf_tag->setAttribute(SHARED_RATING, (uint)WMRatings[ rating + 1 ]);
            }
            if(changedflag & CHANGED_BPM_TAG) {
                asf_tag->setAttribute(BPM_TAG, (uint)beats_per_minute);
            }
            // The Labels
            if(changedflag & CHANGED_TRACK_LABELS)
                check_asf_label_frame(asf_tag, TRACK_LABELS, track_labels_string);
            if(changedflag & CHANGED_ARTIST_LABELS)
                check_asf_label_frame(asf_tag, ARTIST_LABELS, artist_labels_string);
            if(changedflag & CHANGED_ALBUM_LABELS)
                check_asf_label_frame(asf_tag, ALBUM_LABELS, album_labels_string);
            save_base_tags((Tag *)asf_tag);
        }
    }
    return Info::save();
}




ImageList AsfInfo::get_images() const {
    ImageList images;
    if(asf_tag) {
        if(asf_tag->attributeListMap().contains(PICTURE_FLAG)) {
            ASF::AttributeList list = asf_tag->attributeListMap()[ PICTURE_FLAG ];
            if(0 >= list.size())
                return images;
            ASF::AttributeList::ConstIterator it = list.end();
            for(ASF::AttributeList::ConstIterator it = list.begin(); it != list.end() ; it++) {
                ASF::Picture pict = (*it).toPicture();
                if(pict.isValid()) {
                    ByteVector pict_data = pict.picture();
                    Image * image = new Image();
                    if(pict_data.size() > 0) {
                        image->set_data(pict_data);
                        
                        String mimetype = pict.mimeType();
                        if(mimetype.find("/jpeg") != -1 || mimetype.find("/jpg") != -1)
                            image->set_file_type(Image::TYPE_JPEG);
                        else if(mimetype.find("/png") != -1)
                            image->set_file_type(Image::TYPE_PNG);
                        else if(mimetype.find("/bmp") != -1)
                            image->set_file_type(Image::TYPE_BMP);
                        else if(mimetype.find("/gif") != -1)
                            image->set_file_type(Image::TYPE_GIF);
                        
                        image->set_content_type(get_content_type_from_pic_type(pict.type())); 
                        image->set_description(pict.description());
                        images.prepend(image);
                    }
                }
            } 
        }
    }
    return images;
}

void AsfInfo::set_images(const ImageList images) {
    if(asf_tag) {
        if(asf_tag->attributeListMap().contains(PICTURE_FLAG))
            asf_tag->removeItem(PICTURE_FLAG);
        
        
        for(ImageList::ConstIterator it = images.begin(); it != images.end(); ++it) {
//        for(int p = 0; p < image_count; p++) {
            //images[p];
            const Image * image = (*it);
            if( image->get_data().isEmpty())
                continue;
            ASF::Picture picture = ASF::Picture();
            if(image->get_file_type() == Image::TYPE_JPEG) //default to jpeg
                picture.setMimeType("image/jpeg");
            else if(image->get_file_type() == Image::TYPE_PNG)
                picture.setMimeType("image/png");
            else if(image->get_file_type() == Image::TYPE_GIF)
                picture.setMimeType("image/gif");
            else if(image->get_file_type() == Image::TYPE_BMP)
                picture.setMimeType("image/bmp");
            
            ByteVector img_vect(image->get_data());
            picture.setDescription(image->get_description());
            picture.setPicture(img_vect);
            picture.setType(image_type_to_picture_type(image->get_content_type()));
            ASF::Attribute attr = ASF::Attribute(picture);
            asf_tag->addAttribute(PICTURE_FLAG, attr);
        }
    }
}


String AsfInfo::get_lyrics(void) const {
    if(asf_tag) {
        if(asf_tag->attributeListMap().contains(LYRICS))
            return asf_tag->attributeListMap()[ LYRICS ].front().toString();
    }
    return String();
}


void AsfInfo::set_lyrics(const String &lyrics) {
    if(asf_tag) {
        asf_tag->removeItem(LYRICS);
        if(!lyrics.isEmpty())
            asf_tag->setAttribute(LYRICS, lyrics);
    }
}



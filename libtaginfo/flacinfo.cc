/*
 * Copyright (C) 2008-2013 J.Rios <anonbeat@gmail.com>
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Matias De lellis <mati86dl@gmail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */



#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_xiphtags.h"


#include <flacfile.h>
#include <flacpicture.h>


using namespace TagInfo;



FlacInfo::FlacInfo(const String &filename) : XiphInfo(filename) {
    if(file_name.isEmpty()) {
        valid = false;
        flac_file = NULL;
        printf("File name empty!\n");
    }
    else {
        taglib_file = new FLAC::File(file_name.toCString(false), true, AudioProperties::Fast);
        if(taglib_file) {
            if(!taglib_file->isOpen()) {
                cout << "Cannot open file '" << file_name << "'" << endl;
                valid = false;
                return;
            }
            flac_file = ((FLAC::File *) taglib_file);
        }
        else {
            printf("File creation failed for '%s'\n", file_name.toCString(false));
            printf("Cant get xiphcomment from '%s'\n", file_name.toCString(false));
        }
    }
    if(flac_file) {
        xiphcomment = flac_file->xiphComment(); 
        if(!xiphcomment || xiphcomment->isEmpty()) {
            taglib_tag = flac_file->tag();
            if(!taglib_tag) {
                printf("Cant get tag object from '%s'\n", file_name.toCString(false));
                valid = false;
            }
        }
    }
    else {
        xiphcomment = NULL;
        valid = false;
    }
}


FlacInfo::~FlacInfo() {
    //std::cout << "FlacInfo dtor" << std::endl;
}



bool FlacInfo::load(void) {
    if(XiphInfo::load()) {
        if(flac_file) {
            List<FLAC::Picture *> plist = flac_file->pictureList();
            if(plist.size() > 0)
                has_image = true;
        }
        return true;
    }
    return false;
}

Image::ContentType flac_image_type_to_image_type(const FLAC::Picture::Type &tpe) {
    switch(tpe) {
        case FLAC::Picture::FrontCover:
            return Image::CONTENT_COVER_FRONT;
        case FLAC::Picture::BackCover:
            return Image::CONTENT_COVER_BACK;
        case FLAC::Picture::Other:
            return Image::CONTENT_OTHER;
        case FLAC::Picture::FileIcon:
            return Image::CONTENT_FILE_ICON;
        case FLAC::Picture::OtherFileIcon:
            return Image::CONTENT_OTHER_FILE_ICON;
        case FLAC::Picture::LeafletPage:
            return Image::CONTENT_LEAFLET_PAGE;
        case FLAC::Picture::Media:
            return Image::CONTENT_MEDIA;
        case FLAC::Picture::LeadArtist:
            return Image::CONTENT_LEAD_ARTIST;
        case FLAC::Picture::Artist:
            return Image::CONTENT_ARTIST;
        case FLAC::Picture::Conductor:
            return Image::CONTENT_CONDUCTOR;
        case FLAC::Picture::Band:
            return Image::CONTENT_BAND;
        case FLAC::Picture::Composer:
            return Image::CONTENT_COMPOSER;
        case FLAC::Picture::Lyricist:
            return Image::CONTENT_LYRICIST;
        case FLAC::Picture::RecordingLocation:
            return Image::CONTENT_RECORDING_LOCATION;
        case FLAC::Picture::DuringRecording:
            return Image::CONTENT_DURING_RECORDING;
        case FLAC::Picture::DuringPerformance:
            return Image::CONTENT_DURING_PERFORMANCE;
        case FLAC::Picture::MovieScreenCapture:
            return Image::CONTENT_MOVIESCREENCAPTURE;
        case FLAC::Picture::ColouredFish:
            return Image::CONTENT_COLOURED_FISH;
        case FLAC::Picture::Illustration:
            return Image::CONTENT_ILLUSTRATION;
        case FLAC::Picture::BandLogo:
            return Image::CONTENT_ARTIST_LOGO;
        case FLAC::Picture::PublisherLogo:
            return Image::CONTENT_PUBLISHER_LOGO;
        default:
            return Image::CONTENT_OTHER;
    }
}

ImageList FlacInfo::get_images() const {
    ImageList images;
    if(flac_file) {
        List<FLAC::Picture *> plist = flac_file->pictureList();
        if(plist.size() > 0) {
            for(List<FLAC::Picture *>::Iterator it = plist.begin(); it != plist.end(); ++it) {
                Image * image = new Image();
                FLAC::Picture * p = (*it);
                image->set_content_type(flac_image_type_to_image_type(p->type()));
                image->set_description(p->description());
                if(!p->data().isEmpty()) {
                    image->set_data(p->data());
                    images.append(image);
                }
                else {
                    delete image;
                }
            }
        }
    }
    return images;
}


void FlacInfo::set_images(const ImageList images) {
    if(flac_file) {
        flac_file->removePictures();
        
        for(ImageList::ConstIterator it = images.begin(); it != images.end(); ++it) {
            if((*it)->get_data().isEmpty())
                continue;
            ByteVector vect((*it)->get_data());
            FLAC::Picture * picture = new FLAC::Picture();
            if(!picture) {
                std::cout << "could not create picture" << std::endl;
                continue;
            }
            picture->setData(vect);
            picture->setDescription((*it)->get_description());
            picture->setMimeType("image/jpeg"); //TODO
            picture->setType((FLAC::Picture::Type) (*it)->get_content_type());
            flac_file->addPicture(picture);
        }
    }
}

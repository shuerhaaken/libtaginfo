/*
 * Copyright(C) 2008-2013 J.Rios <anonbeat@gmail.com>
 * Copyright(C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or(at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Matias De lellis <mati86dl@gmail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */



#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_id3tags.h"

#include <string>
#include <stdio.h>

#include <id3v2tag.h>
#include <popularimeterframe.h>
#include <textidentificationframe.h>
#include <unsynchronizedlyricsframe.h>
#include <urllinkframe.h>


#define LYRICS              "USLT"
#define IMAGES              "APIC"
#define VOLUME              "TPOS"
#define TRACKNUMBER         "TRCK"
#define COMPOSER            "TCOM"
#define ORIGINAL_ARTIST     "TOPE"
#define ALBUM_ARTIST        "TPE2"
#define COMPILATION         "TCMP"
#define COPYRIGHT           "TCOP"
#define ENCODER             "TENC"
#define HOMEPAGE            "WXXX"
#define TRACK_LABELS        "TRACK_LABELS"
#define ALBUM_LABELS        "ALBUM_LABELS"
#define ARTIST_LABELS       "ARTIST_LABELS"
#define BPM_TAG             "TBPM"



using namespace TagInfo;


// Common stuff


void set_user_textid_frame(ID3v2::Tag * tagv2, const String& description, const String &value) {
    ID3v2::UserTextIdentificationFrame * frame;
    frame = ID3v2::UserTextIdentificationFrame::find(tagv2, description);
    if(!frame) {
        frame = new ID3v2::UserTextIdentificationFrame();
        frame->setDescription(description);
        tagv2->addFrame(frame);
    }
    if(frame)
        frame->setText(value);
}


ID3v2::PopularimeterFrame * get_popularity_frame(ID3v2::Tag * tag, const String &email) {
    ID3v2::FrameList popularity_frame_list = tag->frameList("POPM");
    for(ID3v2::FrameList::Iterator it = popularity_frame_list.begin(); it != popularity_frame_list.end(); ++it) {
        ID3v2::PopularimeterFrame * popularity_frame = static_cast<ID3v2::PopularimeterFrame *>(* it);
        if(email.isEmpty() ||(popularity_frame->email() == email))
            return popularity_frame;
    }
    return NULL;
}



ID3v2::AttachedPictureFrame::Type image_type_to_picframetype(const Image::ContentType &image_type) {
    switch(image_type) {
        case Image::CONTENT_COVER_FRONT :
            return ID3v2::AttachedPictureFrame::FrontCover;
        case Image::CONTENT_COVER_BACK :
            return ID3v2::AttachedPictureFrame::BackCover;
        case Image::CONTENT_OTHER :
            return ID3v2::AttachedPictureFrame::Other;
        case Image::CONTENT_FILE_ICON :
            return ID3v2::AttachedPictureFrame::FileIcon;
        case Image::CONTENT_OTHER_FILE_ICON :
            return ID3v2::AttachedPictureFrame::OtherFileIcon;
        case Image::CONTENT_LEAFLET_PAGE :
            return ID3v2::AttachedPictureFrame::LeafletPage;
        case Image::CONTENT_MEDIA  :
            return ID3v2::AttachedPictureFrame::Media;
        case Image::CONTENT_LEAD_ARTIST :
            return ID3v2::AttachedPictureFrame::LeadArtist;
        case Image::CONTENT_ARTIST :
            return ID3v2::AttachedPictureFrame::Artist;
        case Image::CONTENT_CONDUCTOR :
            return ID3v2::AttachedPictureFrame::Conductor;
        case Image::CONTENT_BAND :
            return ID3v2::AttachedPictureFrame::Band;
        case Image::CONTENT_COMPOSER :
            return ID3v2::AttachedPictureFrame::Composer;
        case Image::CONTENT_LYRICIST :
            return ID3v2::AttachedPictureFrame::Lyricist;
        case Image::CONTENT_RECORDING_LOCATION :
            return ID3v2::AttachedPictureFrame::RecordingLocation;
        case Image::CONTENT_DURING_RECORDING :
            return ID3v2::AttachedPictureFrame::DuringRecording;
        case Image::CONTENT_DURING_PERFORMANCE :
            return ID3v2::AttachedPictureFrame::DuringPerformance;
        case Image::CONTENT_MOVIESCREENCAPTURE :
            return ID3v2::AttachedPictureFrame::MovieScreenCapture;
        case Image::CONTENT_COLOURED_FISH :
            return ID3v2::AttachedPictureFrame::ColouredFish;
        case Image::CONTENT_ILLUSTRATION :
            return ID3v2::AttachedPictureFrame::Illustration;
        case Image::CONTENT_ARTIST_LOGO :
            return ID3v2::AttachedPictureFrame::BandLogo;
        case Image::CONTENT_PUBLISHER_LOGO :
            return ID3v2::AttachedPictureFrame::PublisherLogo;
        default:
            return ID3v2::AttachedPictureFrame::Other;
    }
}


// ID3


Id3Info::Id3Info(const String &filename) : Info(filename) {
    taglib_tagId3v2 = NULL;
}


Id3Info::~Id3Info() {
}


bool Id3Info::load(void) {
    if(Info::load()) {
            // If its a ID3v2 Tag try to load the labels
        if(taglib_tagId3v2 && !taglib_tagId3v2->isEmpty()) {
            if(!taglib_tag)
                load_base_tags((Tag *)taglib_tagId3v2);
            if(taglib_tagId3v2->frameListMap().contains(VOLUME) && !taglib_tagId3v2->frameListMap()[VOLUME].isEmpty()) {
                StringList list = taglib_tagId3v2->frameListMap()[ VOLUME ].front()->toString().split("/");
                if(list.size() == 2) {
                    volume_number = list.front().toInt();
                    volume_count  = list.back().toInt();
                }
                else if(list.size() == 1) {
                    volume_number = list.front().toInt();
                }
            }
            if(taglib_tagId3v2->frameListMap().contains(TRACKNUMBER) && !taglib_tagId3v2->frameListMap()[TRACKNUMBER].isEmpty()) {
                StringList list = taglib_tagId3v2->frameListMap()[ TRACKNUMBER ].front()->toString().split("/");
                if(list.size() == 2)
                    track_count = list.back().toInt();
            }
            if(taglib_tagId3v2->frameListMap().contains(COMPOSER)) {
                composer = taglib_tagId3v2->frameListMap()[ COMPOSER ].front()->toString();
            }
            if(taglib_tagId3v2->frameListMap().contains(ORIGINAL_ARTIST)) {
                original_artist = taglib_tagId3v2->frameListMap()[ ORIGINAL_ARTIST ].front()->toString();
            }
            if(taglib_tagId3v2->frameListMap().contains(ALBUM_ARTIST)) {
                album_artist = taglib_tagId3v2->frameListMap()[ ALBUM_ARTIST ].front()->toString();
            }
            if(taglib_tagId3v2->frameListMap().contains(COMPILATION)) {
                is_compilation =(taglib_tagId3v2->frameListMap()[COMPILATION].front()->toString()) == String("1");
            }
            if(taglib_tagId3v2->frameListMap().contains(COPYRIGHT)) {
                copyright = taglib_tagId3v2->frameListMap()[ COPYRIGHT ].front()->toString();
            }
            if(taglib_tagId3v2->frameListMap().contains(BPM_TAG)) {
                beats_per_minute = taglib_tagId3v2->frameListMap()[ BPM_TAG ].front()->toString().toInt();
            }
            if(taglib_tagId3v2->frameListMap().contains(ENCODER)) {
                encoder = taglib_tagId3v2->frameListMap()[ ENCODER ].front()->toString();
            }
            if(taglib_tagId3v2->frameListMap().contains(HOMEPAGE)) {
                ID3v2::FrameList l = taglib_tagId3v2->frameList(HOMEPAGE);
                ID3v2::UserUrlLinkFrame *f = 
                    dynamic_cast<ID3v2::UserUrlLinkFrame *>(l.front());
                homepage = f->url();
            }
            
            ID3v2::PopularimeterFrame * popularity_frame = NULL;
            
            popularity_frame = get_popularity_frame(taglib_tagId3v2, "LibTagInfo");
            if(!popularity_frame)
                popularity_frame = get_popularity_frame(taglib_tagId3v2, "");
            
            if(popularity_frame) {
                rating    = popularity_to_rating(popularity_frame->rating());
                playcount = popularity_frame->counter();
            }
            if(track_labels.size() == 0) {
                ID3v2::UserTextIdentificationFrame * frame = ID3v2::UserTextIdentificationFrame::find(taglib_tagId3v2, TRACK_LABELS);
                if(!frame)
                    frame = ID3v2::UserTextIdentificationFrame::find(taglib_tagId3v2, "TRACKLABELS");
                if(frame) {
                    StringList track_labels_list = frame->fieldList();
                    if(track_labels_list.size()) {
                        track_labels_string = track_labels_list[1];
                        track_labels = split(track_labels_string, "|");
                    }
                }
            }
            if(artist_labels.size() == 0) {
                ID3v2::UserTextIdentificationFrame * frame = 
                    ID3v2::UserTextIdentificationFrame::find(taglib_tagId3v2, ARTIST_LABELS);
                if(!frame)
                    frame = ID3v2::UserTextIdentificationFrame::find(taglib_tagId3v2, "ARTISTLABELS");
                if(frame) {
                    StringList artist_labels_list = frame->fieldList();
                    if(artist_labels_list.size()) {
                        artist_labels_string = artist_labels_list[1];
                        artist_labels = split(artist_labels_string, "|");
                    }
                }
            }
            if(album_labels.size() == 0) {
                ID3v2::UserTextIdentificationFrame * frame = ID3v2::UserTextIdentificationFrame::find(taglib_tagId3v2, ALBUM_LABELS);
                if(!frame)
                    frame = ID3v2::UserTextIdentificationFrame::find(taglib_tagId3v2, "ALBUMLABELS");
                if(frame) {
                    StringList album_labels_list = frame->fieldList();
                    if(album_labels_list.size()) {
                        album_labels_string = album_labels_list[1];
                        album_labels = split(album_labels_string, "|");
                    }
                }
            }
            // Image availability
            ID3v2::FrameList frame_list = taglib_tagId3v2->frameListMap()[IMAGES];
            has_image =(frame_list.size() > 0); //something is there
        }
    }
    else {
          cout  << "Error: Could not load tags from file '" << file_name.toCString(true) << "'" <<  endl;
        return false; 
    }
    return true;
}




bool Id3Info::save(void) {
    if(taglib_tagId3v2) {
        if(changedflag) {
            ID3v2::TextIdentificationFrame * frame;
            if(changedflag & CHANGED_DATA_VOL_NUM || changedflag & CHANGED_DATA_VOL_CNT) {
                taglib_tagId3v2->removeFrames(VOLUME);
                frame = new ID3v2::TextIdentificationFrame(VOLUME);
                String v;
                v += String::number(volume_number);
                v += String('/');
                v += String::number(volume_count);
                frame->setText(v);
                taglib_tagId3v2->addFrame(frame);
            }
            if(changedflag & CHANGED_COMPOSER_TAG) {
                taglib_tagId3v2->removeFrames(COMPOSER);
                frame = new ID3v2::TextIdentificationFrame(COMPOSER);
                frame->setText(composer);
                taglib_tagId3v2->addFrame(frame);
            }
            if(changedflag & CHANGED_TRACK_NUMBER) {
                taglib_tagId3v2->removeFrames(TRACKNUMBER);
                if(!(changedflag & CHANGED_TRACK_COUNT)) {
                    // prevent overwrite in save_base_tags()
                    changedflag &= ~(CHANGED_TRACK_NUMBER);
                    taglib_tagId3v2->setTrack(track_number);
                }
                else {
                    // prevent overwrite in save_base_tags()
                    changedflag &= ~(CHANGED_TRACK_NUMBER);
                    changedflag &= ~(CHANGED_TRACK_COUNT);
                    frame = new ID3v2::TextIdentificationFrame(TRACKNUMBER);
                    String v;
                    v += String::number(track_number);
                    v += String('/');
                    v += String::number(track_count);
                    frame->setText(v);
                    taglib_tagId3v2->addFrame(frame);
                }
            }
            if(changedflag & CHANGED_TRACK_COUNT) {
                // prevent overwrite in save_base_tags()
                changedflag &= ~(CHANGED_TRACK_NUMBER);
                changedflag &= ~(CHANGED_TRACK_COUNT);
                taglib_tagId3v2->removeFrames(TRACKNUMBER);
                frame = new ID3v2::TextIdentificationFrame(TRACKNUMBER);
                String v;
                v += String::number(track_number);
                v += String('/');
                v += String::number(track_count);
                frame->setText(v);
                taglib_tagId3v2->addFrame(frame);
            }
            if(changedflag & CHANGED_ORIGINALARTIST_TAG) {
                taglib_tagId3v2->removeFrames(ORIGINAL_ARTIST);
                frame = new ID3v2::TextIdentificationFrame(ORIGINAL_ARTIST);
                frame->setText(original_artist);
                taglib_tagId3v2->addFrame(frame);
            }
            if(changedflag & CHANGED_DATA_ALBUMARTIST) {
                taglib_tagId3v2->removeFrames(ALBUM_ARTIST);
                frame = new ID3v2::TextIdentificationFrame(ALBUM_ARTIST);
                frame->setText(album_artist);
                taglib_tagId3v2->addFrame(frame);
            }
            if(changedflag & CHANGED_IS_COMPILATION_TAG) {
                taglib_tagId3v2->removeFrames(COMPILATION);
                frame = new ID3v2::TextIdentificationFrame(COMPILATION);
                if(is_compilation)
                    frame->setText("1");
                else
                    frame->setText("0");
                taglib_tagId3v2->addFrame(frame);
            }
            ID3v2::PopularimeterFrame * popularity_frame;
            if(changedflag & CHANGED_DATA_RATING) {
                popularity_frame = get_popularity_frame(taglib_tagId3v2, "LibTagInfo");
                if(!popularity_frame) {
                    popularity_frame = new ID3v2::PopularimeterFrame();
                    taglib_tagId3v2->addFrame(popularity_frame);
                    popularity_frame->setEmail("LibTagInfo");
                }
                if(popularity_frame)
                    popularity_frame->setRating(rating_to_popularity(rating));
            }
            if(changedflag & CHANGED_DATA_PLAYCOUNT) {
                if(!popularity_frame)
                    popularity_frame = get_popularity_frame(taglib_tagId3v2, "LibTagInfo");
                if(!popularity_frame) {
                    popularity_frame = new ID3v2::PopularimeterFrame();
                    taglib_tagId3v2->addFrame(popularity_frame);
                    popularity_frame->setEmail("LibTagInfo");
                }
                if(popularity_frame)
                    popularity_frame->setCounter(playcount);
            }
            
            if(changedflag & CHANGED_COPYRIGHT_TAG) {
                taglib_tagId3v2->removeFrames(COPYRIGHT);
                frame = new ID3v2::TextIdentificationFrame(COPYRIGHT);
                frame->setText(copyright);
                taglib_tagId3v2->addFrame(frame);
            }
            if(changedflag & CHANGED_BPM_TAG) {
                taglib_tagId3v2->removeFrames(BPM_TAG);
                frame = new ID3v2::TextIdentificationFrame(BPM_TAG);
                frame->setText(String::number(beats_per_minute));
                taglib_tagId3v2->addFrame(frame);
            }
            if(changedflag & CHANGED_ENCODER_TAG) {
                taglib_tagId3v2->removeFrames(ENCODER);
                frame = new ID3v2::TextIdentificationFrame(ENCODER);
                frame->setText(encoder);
                taglib_tagId3v2->addFrame(frame);
            }
            if(changedflag & CHANGED_HOMEPAGE_TAG) {
                taglib_tagId3v2->removeFrames(HOMEPAGE);
                ID3v2::UserUrlLinkFrame *f = new ID3v2::UserUrlLinkFrame();
                f->setUrl(homepage);
                taglib_tagId3v2->addFrame(f);
            }
            
            // The Labels
            if(changedflag & CHANGED_TRACK_LABELS)
                set_user_textid_frame(taglib_tagId3v2, TRACK_LABELS, track_labels_string);
            if(changedflag & CHANGED_ARTIST_LABELS)
                set_user_textid_frame(taglib_tagId3v2, ARTIST_LABELS, artist_labels_string);
            if(changedflag & CHANGED_ALBUM_LABELS)
                set_user_textid_frame(taglib_tagId3v2, ALBUM_LABELS, album_labels_string);
            
            save_base_tags((Tag *)taglib_tagId3v2);
        }
    }
    return Info::save();
}

Image::ContentType get_content_type_from_pic_frame_type(const ID3v2::AttachedPictureFrame::Type &tpe) {
    switch(tpe) {
        case ID3v2::AttachedPictureFrame::FrontCover:
            return Image::CONTENT_COVER_FRONT;
        case ID3v2::AttachedPictureFrame::BackCover:
            return Image::CONTENT_COVER_BACK;
        case ID3v2::AttachedPictureFrame::Other:
            return Image::CONTENT_OTHER;
        case ID3v2::AttachedPictureFrame::FileIcon:
            return Image::CONTENT_FILE_ICON;
        case ID3v2::AttachedPictureFrame::OtherFileIcon:
            return Image::CONTENT_OTHER_FILE_ICON;
        case ID3v2::AttachedPictureFrame::LeafletPage:
            return Image::CONTENT_LEAFLET_PAGE;
        case ID3v2::AttachedPictureFrame::Media:
            return Image::CONTENT_MEDIA;
        case ID3v2::AttachedPictureFrame::LeadArtist:
            return Image::CONTENT_LEAD_ARTIST;
        case ID3v2::AttachedPictureFrame::Artist:
            return Image::CONTENT_ARTIST;
        case ID3v2::AttachedPictureFrame::Conductor:
            return Image::CONTENT_CONDUCTOR;
        case ID3v2::AttachedPictureFrame::Band:
            return Image::CONTENT_BAND;
        case ID3v2::AttachedPictureFrame::Composer:
            return Image::CONTENT_COMPOSER;
        case ID3v2::AttachedPictureFrame::Lyricist:
            return Image::CONTENT_LYRICIST;
        case ID3v2::AttachedPictureFrame::RecordingLocation:
            return Image::CONTENT_RECORDING_LOCATION;
        case ID3v2::AttachedPictureFrame::DuringRecording:
            return Image::CONTENT_DURING_RECORDING;
        case ID3v2::AttachedPictureFrame::DuringPerformance:
            return Image::CONTENT_DURING_PERFORMANCE;
        case ID3v2::AttachedPictureFrame::MovieScreenCapture:
            return Image::CONTENT_MOVIESCREENCAPTURE;
        case ID3v2::AttachedPictureFrame::ColouredFish:
            return Image::CONTENT_COLOURED_FISH;
        case ID3v2::AttachedPictureFrame::Illustration:
            return Image::CONTENT_ILLUSTRATION;
        case ID3v2::AttachedPictureFrame::BandLogo:
            return Image::CONTENT_ARTIST_LOGO;
        case ID3v2::AttachedPictureFrame::PublisherLogo:
            return Image::CONTENT_PUBLISHER_LOGO;
        default:
            return Image::CONTENT_OTHER;
    }
}

ImageList Id3Info::get_images() const {
    ImageList images;
    if(taglib_tagId3v2) {
        ID3v2::FrameList framelist = taglib_tagId3v2->frameListMap()[IMAGES];
        ID3v2::AttachedPictureFrame * pic_frame = NULL;
        int cnt = framelist.size();
        for(list<ID3v2::Frame*>::iterator iter = framelist.begin(); iter != framelist.end(); iter++) {
            pic_frame = static_cast<ID3v2::AttachedPictureFrame *>(*iter);
            Image * image = new Image();
            image->set_content_type(get_content_type_from_pic_frame_type(pic_frame->type()));
            if(pic_frame->picture().size() > 0) {
                image->set_data(pic_frame->picture());
                String mimetype = pic_frame->mimeType();
                if(mimetype.find("/jpeg") != -1 || mimetype.find("/jpg") != -1)
                    image->set_file_type(Image::TYPE_JPEG);
                else if(mimetype.find("/png") != -1)
                    image->set_file_type(Image::TYPE_PNG);
                else if(mimetype.find("/gif") != -1)
                    image->set_file_type(Image::TYPE_GIF);
                else if(mimetype.find("/bmp") != -1)
                    image->set_file_type(Image::TYPE_BMP);
                image->set_description(pic_frame->description());
            }
            images.append(image);
        }
    }
    return images;
}

void Id3Info::set_images(const ImageList images) {
    if(taglib_tagId3v2) {
        ID3v2::AttachedPictureFrame * PicFrame = NULL;
        ID3v2::FrameList frameList = taglib_tagId3v2->frameListMap()[IMAGES];
        for(list<ID3v2::Frame*>::iterator iter = frameList.begin(); iter != frameList.end(); iter++) {
            PicFrame = static_cast<ID3v2::AttachedPictureFrame *>(*iter);
            taglib_tagId3v2->removeFrame(PicFrame, true);
        }
        for(ImageList::ConstIterator it = images.begin(); it != images.end(); ++it) {
            if((*it)->get_data().isEmpty())
                continue;
            PicFrame = new ID3v2::AttachedPictureFrame;
            if((*it)->get_file_type() == Image::TYPE_JPEG || 
               (*it)->get_file_type() == Image::TYPE_UNKNOWN) //default to jpeg
                PicFrame->setMimeType("image/jpeg");
            else if((*it)->get_file_type() == Image::TYPE_PNG)
                PicFrame->setMimeType("image/png");
            else if((*it)->get_file_type() == Image::TYPE_GIF)
                PicFrame->setMimeType("image/gif");
            else if((*it)->get_file_type() == Image::TYPE_BMP)
                PicFrame->setMimeType("image/bmp");
            PicFrame->setType(image_type_to_picframetype((*it)->get_content_type()));
            if(!(*it)->get_description().isEmpty())
                PicFrame->setDescription((*it)->get_description());
            ByteVector img_vect((*it)->get_data());
            PicFrame->setPicture(img_vect);
            taglib_tagId3v2->addFrame(PicFrame);
        }
    }
}


String Id3Info::get_lyrics(void) const {
    if(taglib_tagId3v2) {
        ID3v2::FrameList frameList = taglib_tagId3v2->frameList(LYRICS);
        if(!frameList.isEmpty()) {
            ID3v2::UnsynchronizedLyricsFrame * lyrics_frame = 
                static_cast<ID3v2::UnsynchronizedLyricsFrame * >(frameList.front());
            if(lyrics_frame) {
                return lyrics_frame->text();
            }
        }
        return String();
    }
    return String();
}


void Id3Info::set_lyrics(const String &lyrics) {
    if(taglib_tagId3v2) {
        ID3v2::UnsynchronizedLyricsFrame * lyrics_frame;
        ID3v2::FrameList frameList = taglib_tagId3v2->frameListMap()[LYRICS];
        for(list<ID3v2::Frame*>::iterator iter = frameList.begin(); iter != frameList.end(); iter++) {
            lyrics_frame = static_cast<ID3v2::UnsynchronizedLyricsFrame*>(*iter);
            taglib_tagId3v2->removeFrame(lyrics_frame, true);
        }
        if(!lyrics.isEmpty()) {
            lyrics_frame = new ID3v2::UnsynchronizedLyricsFrame();
            lyrics_frame->setText(lyrics);
            taglib_tagId3v2->addFrame(lyrics_frame);
        }
    }
}







/*
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 */


#include "taginfo.h"
#include "taginfo_internal.h"


using namespace TagInfo;

//! Constructor
Image::Image() {
    image_type      = Image::CONTENT_OTHER;
    image_file_type = Image::TYPE_UNKNOWN;
    description     = String::null;
}

//! Destructor
Image::~Image() {
}

//! Get the image binary data from the Image object.
//! \return ByteVector with image data
const ByteVector Image::get_data() const {
    return bytes;
}

//! Get the image binary data from the Image object.
//! \param image_data_length Return locatio for the length of the returned data array 
//! \return char * with image data
char * Image::get_data(uint &image_data_length) const {
    image_data_length = bytes.size();
    char * idat = new char[bytes.size()];
    memcpy(idat, bytes.data(), bytes.size());
    return idat;
}

//! Set the image binary data in the Image object.
//! \param image_data ByteVector with image data
void Image::set_data(const ByteVector &image_data) {
    bytes = image_data;
}

//! Set the image binary data in the Image object.
//! \param image_data_length length of the data array 
//! \param image_data Image binary data
void Image::set_data(const char * image_data, const uint image_data_length) {
    ByteVector v(image_data, image_data_length);
    bytes = v;
}

//! Get the image content's type from the Image object.
//! \return type as ImageType
Image::ContentType Image::get_content_type(void) const {
    return image_type;
}
//! Set the image content's type in the Image object.
//! \param it as ImageType
void Image::set_content_type(const Image::ContentType it) {
    image_type = it;
}

//! Get the image file's type from the Image object.
//! \return type as ImageFileType
Image::FileType Image::get_file_type(void) const {
    return image_file_type;
}
//! Set the image file's type in the Image object.
//! \param ft as ImageFileType
void Image::set_file_type(const Image::FileType ft) {
    image_file_type = ft;
}

//! Get the image file's description from the Image object.
//! \return Description as String
const String Image::get_description(void) const {
    /*if(description == String::null)
        return String();*/
    return description;
}
//! Set the image file's description in the Image object.
//! \param new_description as String
void Image::set_description(const String &new_description) {
    description = new_description;
}



/*
 * Copyright (C) 2008-2013 J.Rios <anonbeat@gmail.com>
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Matias De lellis <mati86dl@gmail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */


#include <algorithm>
#include "taginfo.h"
#include "taginfo_internal.h"

#include "taginfo_apetags.h"
#include "taginfo_asftags.h"
#include "taginfo_modtags.h"
#include "taginfo_mp4tags.h"
#include "taginfo_id3tags.h"
#include "taginfo_xiphtags.h"



using namespace TagInfo;



/*! \mainpage TagInfo Documentation
 * 
 * <b>libtaginfo is a wrapper library for taglib</b> and alows access to media tags in an abstract way. 
 * 
 * libtaginfo is a C++ library.
 * 
 * C and vala bindings are available with this library.
 * 
 * Known tag formats: 
 *  - Ape tags, 
 *  - Asf tags and 
 *  - Id3 tags (v1, v2.2, v2.3, v2.4),
 *  - Mod tags.
 *  - Mp4 tags, 
 *  - Xiphcomment/Vorbis tag, 
 * 
 * These tags are used with the following media files (extensions / mime):
 *  - aac  : audio/aac, audio/aacp, audio/mp4, audio/x-aac
 *  - aif  : audio/x-aiff
 *  - aiff : audio/x-aiff
 *  - ape  : application/x-ape
 *  - asf  : audio/x-ms-asf, video/x-ms-asf
 *  - flac : audio/flac, audio/x-flac+ogg, audio/x-flac
 *  - it   : audio/x-it
 *  - m4a  : audio/mp4a-latm, audio/x-m4a
 *  - m4b  : audio/m4b
 *  - m4p  : audio/x-m4p
 *  - mod  : audio/x-mod
 *  - mp3  : audio/mpeg, audio/x-mpegurl
 *  - mp4  : audio/mp4, video/mp4
 *  - mpc  : audio/mpc, audio/x-musepack
 *  - oga  : audio/x-vorbis+ogg, audio/ogg
 *  - ogg  : audio/x-vorbis+ogg, audio/ogg
 *  - s3m  : audio/x-s3m
 *  - spx  : audio/x-spx, audio/x-speex, audio/x-speex+ogg
 *  - tta  : audio/tta, audio/x-tta
 *  - wav  : audio/x-wav
 *  - wma  : audio/x-ms-wma
 *  - wmv  : video/x-ms-wmv, video/x-msvideo
 *  - wv   : application/x-wavpack, audio/wavpack, audio/x-wavpack
 *  - xm   : audio/x-xm
 *
 * There is an API for accessing the following media tags:
 *  - Album
 *  - Album artist
 *  - Artist
 *  - BPM
 *  - Comments;
 *  - Compilation/V.A. flag
 *  - Composer;
 *  - Copyright;
 *  - Encoder;
 *  - Genre
 *  - Homepage;
 *  - Images (embedded image files)
 *  - Lyrics
 *  - Original artist
 *  - Play count
 *  - Rating (1 - 5 stars, 0 not set) 
 *  - Title
 *  - Track count;
 *  - Track number;
 *  - User defined album labels;
 *  - User defined artist labels;
 *  - User defined track labels;
 *  - Volume count;
 *  - Volume number;
 *  - Year
 * 
 * However, some media tags are not supported by some tag formats. (E.g. mod tags are very limited)
 * 
 * There is an API for accessing the following audio properties:
 *  - Bitrate
 *  - Channel count
 *  - Lenght in seconds
 *  - Sample rate
 *  
 * 
 * <b>Getting Started</b>
 * 
 * Libtaginfo provides a simple API which makes it possible to ignore the differences between tagging formats and format specific 
 * APIs which allow programmers to work with the features of specific tagging schemes. 
 * 
 * Here's a very simple example with TagLib:
 * 
 *     TagInfo::InfoRef ref("song.mp3");
 *     ref->load();
 *     TagLib::String artist = ref.info()->get_artist(); // artist == "Don van Vliet"
 *     
 *     TagInfo::InfoRef ref2("song.ogg");
 *     ref2.info()->set_album("Mambo No. 1");
 *     ref2.save();
 * 
 * More examples can be found in the <b>examples</b> directory in the source code package.
 * 
 */





/*!
 * Create a Info from \a file. If \a format is given, libtaginfo will try to create 
 * an Info object for the desired \a format.
 * This is a recommended way for accessing media tags through libtaginfo.
 * create_with_file_type() will return a matching Info subclass
 * \return the according Info subclass
 * \param filename is the path to a media file.
 * \param format is the MediaFileType.
 */
Info * Info::create_with_file_type(const String &filename, MediaFileType format) {
    Info * info = NULL;
    switch(format) {
        case  FILE_TYPE_UNKNOWN :
            break;
        case  FILE_TYPE_MP3 :
            info = new Mp3Info(filename);
            break;
        case  FILE_TYPE_FLAC :
            info = new FlacInfo(filename);
            break;
        case  FILE_TYPE_OGG :
        case  FILE_TYPE_OGA :
            info = new OggInfo(filename);
            break;
        case  FILE_TYPE_MP4 :
        case  FILE_TYPE_M4A :
        case  FILE_TYPE_M4B :
        case  FILE_TYPE_M4P :
        case  FILE_TYPE_AAC : 
            info = new Mp4Info(filename);
            break;
        case  FILE_TYPE_WMA :
        case  FILE_TYPE_WMV :
        case  FILE_TYPE_ASF :
            info = new AsfInfo(filename);
            break;
#ifdef TAGLIB_ONE_NINE
        case  FILE_TYPE_OPUS :
            info = new OpusInfo(filename);
            break;
#endif /* TAGLIB_ONE_NINE */
        case FILE_TYPE_APE :
            info = new ApeInfo(filename);
            break;
        case FILE_TYPE_WAV :
            info = new WaveInfo(filename);
            break;
        case FILE_TYPE_AIF :
            info = new AiffInfo(filename);
            break;
        case FILE_TYPE_WV : 
            info = new WavPackInfo(filename);
            break;
        case FILE_TYPE_TTA :
            info = new TrueAudioInfo(filename);
            break;
        case FILE_TYPE_MPC :
            info = new MpcInfo(filename);
            break;
        case FILE_TYPE_SPEEX :
            info = new SpeexInfo(filename);
            break;
        case FILE_TYPE_MOD :
            info = new ModInfo(filename);
            break;
        case FILE_TYPE_S3M :
            info = new S3mInfo(filename);
            break;
        case FILE_TYPE_IT :
            info = new ItInfo(filename);
            break;
        case FILE_TYPE_XM :
            info = new XmInfo(filename);
            break;
        default :
            break;
    }
    if(!info ) {
        std::cout << "LibTagInfo: Could not create from: " << filename << " !" << std::endl;
        return NULL;
    }
    if(!info->is_valid()) {
        std::cout << "LibTagInfo: Could not create valid Info from: " << filename << " !" << std::endl;
        if(!info->is_valid()) {
            std::cout << "LibTagInfo: Info object from " << filename << " is not valid!" << std::endl;
        }
        return NULL;
    }
    return info;
}

/*!
 * Create a Info object from a media file.
 *
 * This is a recommended way for accessing media tags through libtaginfo.
 * create() will return a matching Info subclass
 * \return the according Info subclass
 * \param filename is the path to a media file.
 *
 * This function will make use of the file extension to find out the media type
 */
Info * Info::create(const String &filename) {
    MediaFileType format = FILE_TYPE_UNKNOWN;
    String fnex = filename.substr(filename.rfind(".") + 1);
    fnex = fnex.upper();
    //std::cout << "fnex: " << fnex << std::endl;
    if(fnex == "MP3")
        format = FILE_TYPE_MP3; 
    else if(fnex == "OGG")
        format = FILE_TYPE_OGG; 
    else if(fnex == "FLAC")
        format = FILE_TYPE_FLAC; 
    else if(fnex == "WMA")
        format = FILE_TYPE_WMA; 
    else if(fnex == "WAV")
        format = FILE_TYPE_WAV; 
    else if(fnex == "APE")
        format = FILE_TYPE_APE; 
    else if(fnex == "OGA")
        format = FILE_TYPE_OGA; 
    else if(fnex == "MP4")
        format = FILE_TYPE_MP4; 
    else if(fnex == "3G2")
        format = FILE_TYPE_MP4; 
    else if(fnex == "M4R")
        format = FILE_TYPE_MP4; 
    else if(fnex == "ASF")
        format = FILE_TYPE_ASF; 
    else if(fnex == "M4A")
        format = FILE_TYPE_M4A; 
    else if(fnex == "M4B")
        format = FILE_TYPE_M4B; 
    else if(fnex == "M4P")
        format = FILE_TYPE_M4P; 
    else if(fnex == "AAC")
        format = FILE_TYPE_AAC; 
    else if(fnex == "AIF")
        format = FILE_TYPE_AIF; 
    else if(fnex == "AIFF")
        format = FILE_TYPE_AIF; 
    else if(fnex == "WV")
        format = FILE_TYPE_WV; 
    else if(fnex == "TTA")
        format = FILE_TYPE_TTA; 
    else if(fnex == "MPC")
        format = FILE_TYPE_MPC; 
    else if(fnex == "SPX")
        format = FILE_TYPE_SPEEX; 
    else if(fnex == "WMV")
        format = FILE_TYPE_WMV;
#ifdef TAGLIB_ONE_NINE
    else if(fnex == "OPUS")
        format = FILE_TYPE_OPUS; 
#endif /* TAGLIB_ONE_NINE */
    else if(fnex == "MOD")
        format = FILE_TYPE_MOD; 
    else if(fnex == "WOW")
        format = FILE_TYPE_MOD; 
    else if(fnex == "MODULE")
        format = FILE_TYPE_MOD; 
    else if(fnex == "NST")
        format = FILE_TYPE_MOD; 
    else if(fnex == "IT")
        format = FILE_TYPE_IT; 
    else if(fnex == "S3M")
        format = FILE_TYPE_S3M; 
    else if(fnex == "XM")
        format = FILE_TYPE_XM; 
    
    return Info::create_with_file_type(filename, format);
}

/*!
 * Create a Info from \param filename . If \param mime_type is given, libtaginfo 
 * will try to create an Info object for the desired mime_type.
 * This is a recommended way for accessing media tags through libtaginfo.
 * create_from_mime() will return a matching Info subclass
 * \return the according Info subclass
 * \param filename is the path to a media file.
 * \param mime_type is the mime type of the media file (e.g. "audio/x-vorbis+ogg") 
 * as String.
 */
Info * Info::create_from_mime(const String &filename, const String &mime_type) {
    MediaFileType format = FILE_TYPE_UNKNOWN;
    String mime = mime_type;
    
    if(mime == "audio/mpeg" || mime == "audio/x-mpegurl")
        format = FILE_TYPE_MP3; 
    else if(mime == "audio/x-vorbis+ogg" || mime == "audio/ogg")
        format = FILE_TYPE_OGG; 
    else if(mime == "audio/flac" || mime == "audio/x-flac+ogg" || mime == "audio/x-flac")
        format = FILE_TYPE_FLAC; 
    else if(mime == "audio/x-ms-wma")
        format = FILE_TYPE_WMA; 
    else if(mime == "video/x-ms-wmv" || mime == "video/x-msvideo")
        format = FILE_TYPE_WMV; 
    else if(mime == "audio/x-spx" || mime == "audio/x-speex" || mime == "audio/x-speex+ogg")
        format = FILE_TYPE_SPEEX;
    else if(mime == "audio/mp4" || mime == "video/mp4")
        format = FILE_TYPE_MP4; 
    else if(mime == "audio/mp4a-latm" || mime == "audio/x-m4a")
        format = FILE_TYPE_M4A; 
    else if(mime == "audio/m4b")
        format = FILE_TYPE_M4B; 
    else if(mime == "audio/x-m4p")
        format = FILE_TYPE_M4P; 
    else if(mime == "audio/aac" || mime == "audio/x-aac" || mime == "audio/x-aac")
        format = FILE_TYPE_AAC; 
    else if(mime == "audio/x-ms-asf" || mime == "video/x-ms-asf")
        format = FILE_TYPE_ASF; 
    else if(mime == "audio/x-wav")
        format = FILE_TYPE_WAV; 
    else if(mime == "audio/ape")
        format = FILE_TYPE_APE; 
    else if(mime == "audio/x-aiff")
        format = FILE_TYPE_AIF; 
#ifdef TAGLIB_ONE_NINE
    else if(mime == "audio/opus" || mime == "application/ogg")
        format = FILE_TYPE_OPUS; 
#endif /* TAGLIB_ONE_NINE */
    else if(mime == "application/x-wavpack" || mime == "audio/wavpack" || mime == "audio/x-wavpack")
        format = FILE_TYPE_WV; 
    else if(mime == "audio/x-mod")
        format = FILE_TYPE_MOD; 
    else if(mime == "audio/x-it")
        format = FILE_TYPE_IT; 
    else if(mime == "audio/x-s3m")
        format = FILE_TYPE_S3M; 
    else if(mime == "audio/x-xm")
        format = FILE_TYPE_XM; 
    else if(mime == "audio/mpc" || mime == "audio/x-musepack")
        format = FILE_TYPE_MPC; 
    else if(mime == "audio/tta" || mime == "audio/x-tta")
        format = FILE_TYPE_TTA; 
    
    return Info::create_with_file_type(filename, format);
}


// Info

Info::Info(const String &filename) {
    
    taglib_fileref  = NULL;
    taglib_file     = NULL;
    taglib_tag      = NULL;
    
    file_name       = filename;
    artist          = String::null;
    title           = String::null;
    album_artist    = String::null;
    album           = String::null;
    genre           = String::null;
    composer        = String::null;
    comments        = String::null;
    homepage        = String::null;
    encoder         = String::null;
    copyright       = String::null;
    original_artist = String::null;
    
    track_number    = 0;
    track_count     = 0;
    volume_number   = 0;
    volume_count    = 0;
    year            = 0;
    length_seconds  = 0;
    bitrate         = 0;
    rating          = 0;
    playcount       = 0;
    beats_per_minute= 0;
    
    is_compilation  = false;
    has_image       = false;
    
    valid           = true;
    changedflag     = CHANGED_DATA_NONE;
}


Info::~Info() {
    //std::cout << "Info dtor" << std::endl;
    if(taglib_fileref)
        delete taglib_fileref;
    if(taglib_file)
        delete taglib_file;
    
    //the tags and properties are owned by the file or fileref
}


bool Info::is_valid() {
    return valid;
}


bool setup_file_ref() {
    return false;
}


void Info::load_base_tags(Tag * tag) {
    if(tag) {
        title        = tag->title();
        artist       = tag->artist();
        album        = tag->album();
        genre        = tag->genre();
        comments     = tag->comment();
        track_number = tag->track();
        year         = tag->year();
    }
}


bool Info::load(void) {
    AudioProperties * properties;
    if(taglib_tag) {
        load_base_tags(taglib_tag);
    }
    
    if(taglib_file && 
       (properties = taglib_file->audioProperties())) {
        length_seconds = properties->length();
        bitrate        = properties->bitrate();
        samplerate     = properties->sampleRate();
        channels       = properties->channels();
        return true;
    }
    else if(taglib_fileref && 
            (properties = taglib_fileref->audioProperties())) {
        length_seconds = properties->length();
        bitrate        = properties->bitrate();
        samplerate     = properties->sampleRate();
        channels       = properties->channels();
        return true;
    }
    printf("Problem with Info::load for %s\n", file_name.toCString(false));
    return false;
}


void Info::save_base_tags(Tag * tag) {
    if(changedflag & CHANGED_TITLE_TAG)
        tag->setTitle(title);
    if(changedflag & CHANGED_ARTIST_TAG)
        tag->setArtist(artist);
    if(changedflag & CHANGED_ALBUM_TAG)
        tag->setAlbum(album);
    if(changedflag & CHANGED_GENRE_TAG)
        tag->setGenre(genre);
    if(changedflag & CHANGED_COMMENT_TAG)
        tag->setComment(comments);
    if(changedflag & CHANGED_TRACK_NUMBER)
        tag->setTrack(track_number);
    if(changedflag & CHANGED_YEAR_TAG)
        tag->setYear(year);
}


bool Info::save() {
    if(taglib_tag && changedflag)
        save_base_tags(taglib_tag);
    
    if(!taglib_file->save())
      return false;
    
    return true;
}


//Tag access functions

//! Set the title tag.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_title is a String.
*/
void Info::set_title(const String new_title) {
    title = new_title;
    changedflag |= CHANGED_TITLE_TAG;
}
//! Get the title of a media track.
/*!
\return The title as %String
*/
String Info::get_title() const {
    if(String::null != title)
        return title;
    else
        return String();
}


/*!
 * Set the genre tag.
 */
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
void Info::set_genre(const String new_genre) {
    genre = new_genre;
    changedflag |= CHANGED_GENRE_TAG;
}
//! Get the genre of a media track.
/*!
\return The genre as String
*/
String Info::get_genre() const {
    if(String::null != genre)
        return genre;
    else
        return String();
}


//! Set volume number of a media track. (like CD 2 of 3)
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param number as int
*/
void Info::set_volume_number(const int number) {
    volume_number = number;
    if(volume_number < 0)
        volume_number = 0;
    changedflag |= CHANGED_DATA_VOL_NUM;
}
//! Get volume number of a media track. (like CD 2 of 3)
/*!
\return The volume number as int
*/
int Info::get_volume_number() const {
    return volume_number;
}

//! Set volume count of a media track. (like CD 2 of 3)
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param count as int
*/
void Info::set_volume_count(const int count) {
    volume_count = count;
    if(volume_count < 0)
        volume_count = 0;
    changedflag |= CHANGED_DATA_VOL_CNT;
}
//! Get volume count of a media track. (like CD 2 of 3)
/*!
\return The volume count as int
*/
int Info::get_volume_count() const {
    return volume_count;
}

//! Set the artist tag of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_artist as String
*/
void Info::set_artist(const String new_artist) {
    artist = new_artist;
    changedflag |= CHANGED_ARTIST_TAG;
}
//! Get the artist tag of a media track.
/*!
\return The artist tag as String
*/
String Info::get_artist() const {
    if(String::null != artist)
        return artist;
    else
        return String();
}

//! Set the original artist tag of a media track.
//! For some implementations this is the performer tag 
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_artist as String
*/
void Info::set_original_artist (const String new_artist) {
    original_artist = new_artist;
    changedflag |= CHANGED_ORIGINALARTIST_TAG;
}
//! Get the original artist tag of a media track.
/*!
\return The original artist tag as String
*/
String Info::get_original_artist() const {
    if(String::null != original_artist)
        return original_artist;
    else
        return String();
}

//! Set the encoder tag of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_encoder as String
*/
void Info::set_encoder(const String new_encoder) {
    encoder = new_encoder;
    changedflag |= CHANGED_ENCODER_TAG;
}
//! Get the encoder tag of a media track.
/*!
\return The encoder tag as String
*/
String Info::get_encoder() const {
    if(String::null != encoder)
        return encoder;
    else
        return String();
}

//! Set the homepage tag of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_homepage as String
*/
void Info::set_homepage(const String new_homepage) {
    homepage = new_homepage;
    changedflag |= CHANGED_HOMEPAGE_TAG;
}
//! Get the homepage tag of a media track.
/*!
\return The homepage tag as String
*/
String Info::get_homepage() const {
    if(String::null != homepage)
        return homepage;
    else
        return String();
}

//! Set the copyright tag of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_copyright as String
*/
void Info::set_copyright(const String new_copyright) {
    copyright = new_copyright;
    changedflag |= CHANGED_COPYRIGHT_TAG;
}
//! Get the copyright tag of a media track.
/*!
\return The copyright tag as String
*/
String Info::get_copyright() const {
    if(String::null != copyright)
        return copyright;
    else
        return String();
}

//! Set wether the media file is part of a compilation/various artists album, or not.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
void Info::set_is_compilation(bool compilation) {
    is_compilation = compilation;
    changedflag |= CHANGED_IS_COMPILATION_TAG;
}
//! Get wether the media file is part of a compilation/various artists album, or not.
bool Info::get_is_compilation() const {
    return is_compilation;
}

//! Set the album artist tag of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_album_artist as String
*/
void Info::set_album_artist(const String new_album_artist) {
    album_artist = new_album_artist;
    changedflag |= CHANGED_DATA_ALBUMARTIST;
}
//! Get the album artist tag of a media track.
/*!
\return The album artist tag as String
*/
String Info::get_album_artist() const {
    if(String::null != album_artist)
        return album_artist;
    else
        return String();
}

//! Set the album tag of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_album as String
*/
void Info::set_album(const String new_album) {
    album = new_album;
    changedflag |= CHANGED_ALBUM_TAG;
}
//! Get the album tag of a media track.
/*!
\return The album tag as String
*/
String Info::get_album() const {
    if(String::null != album)
        return album;
    else
        return String();
}

//! Set the composer tag of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_composer as String
*/
void Info::set_composer(const String new_composer) {
    composer = new_composer;
    changedflag |= CHANGED_COMPOSER_TAG;
}
//! Get the composer tag of a media track.
/*!
\return The composer tag as String
*/
String Info::get_composer() const {
    if(String::null != composer)
        return composer;
    else
        return String();
}

//! Set the comments tag of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_comments as String
*/
void Info::set_comments(const String new_comments) {
    comments = new_comments.stripWhiteSpace();
    changedflag |= CHANGED_COMMENT_TAG;
}
//! Get the comments tag of a media track.
/*!
\return The comments tag as String
*/
String Info::get_comments(void) const {
    if(String::null != comments)
        return comments.stripWhiteSpace();
    else
        return String();
}

//! Set the track number of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_track_number as int
*/
void Info::set_track_number(const int new_track_number) {
    track_number = new_track_number;
    changedflag |= CHANGED_TRACK_NUMBER;
}
//! Get the track_number tag of a media track.
/*!
\return The track_number tag as int
*/
int Info::get_track_number() const {
    return track_number;
}

//! Set the track count of a media track.
//! This is the amount of tracks of the album the media file was taken from.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_track_count as int
*/
void Info::set_track_count(const int new_track_count) {
    track_count = new_track_count;
    changedflag |= CHANGED_TRACK_COUNT;
}
//! Get the track_count tag of a media track.
//! This is the amount of tracks of the album the media file was taken from.
/*!
\return The track_count tag as int
*/
int Info::get_track_count() const {
    return track_count;
}

//! Set the year of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_year as int
*/
void Info::set_year(const int new_year) {
    year = new_year;
    changedflag |= CHANGED_YEAR_TAG;
}
//! Get the year tag of a media track.
/*!
\return The year tag as int
*/
int Info::get_year() const {
    return year;
}

//! Set BPM of a media track.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_bpm speed in BPM as int
*/
void Info::set_beats_per_minute(const int new_bpm) {
    beats_per_minute = new_bpm;
    changedflag |= CHANGED_BPM_TAG;
}
//! Get the BPM of a media track.
/*!
\return The BPM as int
*/
int Info::get_beats_per_minute() const {
    return beats_per_minute;
}

//! Get the number of channels of the media file.
int Info::get_channels() const {
    return channels;
}

//! Get the bitrate of the media file.
int Info::get_bitrate() const {
    return bitrate;
}

//! Get the samplerate of the media file.
int Info::get_samplerate() const {
    return samplerate;
}

//! Get the length of the media file in seconds.
int Info::get_length_seconds() const {
    return length_seconds;
}

//! Get the playcount tag of a media track.
/*!
\return The playcount tag as int
*/
int  Info::get_playcount() const {
    return playcount;
}
//! Set the playcount tag of a media track.
//! Some implementations may not support this tag
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_playcount as int
*/
void Info::set_playcount(const int new_playcount) {
    changedflag |= CHANGED_DATA_PLAYCOUNT;
    playcount = new_playcount;
    if(playcount < 0)
        playcount = 0;
}

//! Get the rating tag of a media track.
//! rating 1 - 5 ;  0 -> not set
/*!
\return The rating tag as int
*/
int  Info::get_rating() const {
    return rating;
}
//! Set the rating tag of a media track.
//! rating 1 - 5 ;  0 -> not set
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
/*!
\param new_rating as integer
*/
void Info::set_rating(const int new_rating) {
    changedflag |= CHANGED_DATA_RATING;
    rating = new_rating;
    if(rating < 0)
        rating = 0;
}

//! Set a list of embedded track labels.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
void Info::set_track_labels_list(const StringList &new_track_labels_list) {
    track_labels = new_track_labels_list;
    track_labels_string = new_track_labels_list.toString("|");
    changedflag |= CHANGED_TRACK_LABELS;
}
//! Get a list of embedded track labels.
StringList Info::get_track_labels_list() const {
    return track_labels;
}

//! Set a list of embedded artist labels.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
void Info::set_artist_labels_list(const StringList &new_artist_labels_list) {
    artist_labels = new_artist_labels_list;
    artist_labels_string = new_artist_labels_list.toString("|");
    changedflag |= CHANGED_ARTIST_LABELS;
}
//! Get a list of embedded artist labels.
StringList Info::get_artist_labels_list() const {
    return artist_labels;
}

//! Set a list of embedded album labels.
//! This function will set the according ChangedFlags flag
//! This will not be saved to file before save() is called on the Info object.
void Info::set_album_labels_list(const StringList &new_album_labels_list) {
    album_labels = new_album_labels_list;
    album_labels_string = new_album_labels_list.toString("|");
    changedflag |= CHANGED_ALBUM_LABELS;
}
//! Get a list of embedded album labels.
StringList Info::get_album_labels_list() const {
    return album_labels;
}

//! Get information on the availability of an image
/*!
 * This information is just a quick lookup if something is there.
\return true if there is image data
*/
bool Info::get_has_image() const {
    return has_image;
}

//! Get an array of images from the Info object.
//! The ownership of the images is on the caller side. You have
//! to destroy all the images in the retured array 
//! \return TagLib::List with images
ImageList Info::get_images() const {
    return ImageList();
}
//! Set an array of Image to the Info object.
//! The ownership of the images remains on the caller side.
//! \param images is an ImageList. Will overwrite any other Image in the
//! Info object 
void Info::set_images(const ImageList images) {
}

//! Get an embedded Lyrics String from the Info object.
//! \return String with lyrics
String Info::get_lyrics() const {
    return String();
}
//! Set an embedded Lyrics String to be saved to Info object.
void Info::set_lyrics(const String &lyrics) {
}


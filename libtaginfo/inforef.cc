/*
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 */


#include "taginfo.h"


using namespace TagInfo;


// InfoRef

/*!
 * Create a InfoRef object from a media file.
 *
 * This will internally create a Info object and takes care of its deletion
 * \param filename is the path to a media file.
 *
 * This is a recommended wrapper for RAII creation.  If you want to use a pointer to InfoRef,
 * then you can also avoid it and use Info directly.
 */
InfoRef::InfoRef(const String &filename) {
    i = NULL;
    i = Info::create(filename);
}

/*!
 * Create a InfoRef object from a media file.
 *
 * This will internally create a Info object and takes care of its deletion
 * \param filename is the path to a media file.
 * \param type MediaFileType of the media file.
 *
 * This is a recommended wrapper for RAII creation.  If you want to use a pointer to InfoRef,
 * then you can also avoid it and use Info directly.
 */
InfoRef::InfoRef(const String &filename, MediaFileType type) {
    i = NULL;
    i = Info::create_with_file_type(filename, type);
}

/*!
 * Create a InfoRef object from a media file.
 *
 * This will internally create a Info object and takes care of its deletion
 * \param filename is the path to a media file.
 * \param mime Mime type of the media file.
 *
 * This is a recommended wrapper for RAII creation.  If you want to use a pointer to InfoRef,
 * then you can also avoid it and use Info directly.
 */
InfoRef::InfoRef(const String &filename, const String &mime) {
    i = NULL;
    i = Info::create_from_mime(filename, mime);
}


/*!
 * Check wether the info object pointers could be created internally
 */
bool InfoRef::is_valid() const {
    return i != NULL && i->is_valid();
}

/*!
 * This function forwards the load call to the internal Info object
 * \return True on success
 */
bool InfoRef::load() const {
    if(!i)
        return false;
    return i->load();
}

/*!
 * This function forwards the save call to the internal Info object
 * \return True on success
 */
bool InfoRef::save() {
    if(!i)
        return false;
    return i->save();
}

/*!
 * Get the Info object for actual tag access
 * \return Pointer to Info
 */
TagInfo::Info * InfoRef::info() const {
    return i;
}

/*!
 * Destructor for InfoRef
 */
InfoRef::~InfoRef() {
    delete i;
}



/*
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 
 */


#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_modtags.h"


#include <modfile.h>
#include <modtag.h>

using namespace TagInfo;



ModTagInfo::ModTagInfo(const String &filename) : Info(filename) {
    taglib_tagMod = NULL;
}


ModTagInfo::~ModTagInfo() {
}


bool ModTagInfo::load(void) {
    if(Info::load()) {
        if(taglib_tagMod && !taglib_tagMod->isEmpty()) {
            if(!taglib_tag)
                load_base_tags((Tag *)taglib_tagMod);
        }
    }
    else {
          cout  << "Error: Could not load tags from file '" << file_name.toCString(true) << "'" <<  endl;
        return false; 
    }
    return true;
}




bool ModTagInfo::save(void) {
    if(taglib_tagMod) {
        if(changedflag) {
            save_base_tags((Tag *)taglib_tagMod);
        }
    }
    return Info::save();
}





/*
 * Copyright (C) 2008-2013 J.Rios <anonbeat@gmail.com>
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Matias De lellis <mati86dl@gmail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */


#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_mp4tags.h"


#include <string>
#include <mp4file.h>
#include <mp4coverart.h>


#define LYRICS              "\xa9lyr"
#define COPYRIGHT           "\xa9" "cpy"
#define HOMEPAGE            "\xa9src"
#define COMPOSER            "\xA9wrt"
#define ORIGINAL_ARTIST     "\xa9prf"
#define ENCODER             "\xa9too"
#define COVER_IMAGES        "covr"
#define VOLUME              "disk"
#define COMPILATION         "cpil"
#define RATING              "----:com.apple.iTunes:RATING"
#define PLAYCOUNT           "----:com.apple.iTunes:PLAY_COUNTER"
#define TRACK_LABELS        "----:com.apple.iTunes:TRACK_LABELS"
#define ARTIST_LABELS       "----:com.apple.iTunes:ARTIST_LABELS"
#define ALBUM_LABELS        "----:com.apple.iTunes:ALBUM_LABELS"
#define TRACKNUMBER         "trkn"
#define ALBUM_ARTIST        "aART"
#define BPM_TAG             "tmpo"


using namespace TagInfo;

// MP4


Mp4Info::Mp4Info(const String &filename) : Info(filename) {
    if(file_name.isEmpty()) {
        valid = false;
        printf("File name empty!\n");
    } else {
        taglib_file = new MP4::File(file_name.toCString(false), true, AudioProperties::Fast);
    }
    if(taglib_file) {
        if(!taglib_file->isOpen()) {
            cout << "Cannot open file '" << file_name << "'" << endl;
            valid = false;
            return;
        }
        mp4_tag = ((MP4::File *) taglib_file)->tag();
        if(!mp4_tag || mp4_tag->isEmpty()) {
            if(!mp4_tag)
                printf("Mp4tag null for %s\n", filename.toCString(false));
            //printf("Use fallback! %s\n", filename.c_str());
            if(!taglib_tag)
                taglib_tag = taglib_file->tag();
            if(!taglib_tag) {
                taglib_fileref = new FileRef(filename.toCString(false),
                                                     true, AudioProperties::Fast);
                taglib_tag = taglib_fileref->tag();
            }
            if(!taglib_tag) {
                printf("Cant get tag object from '%s'\n", file_name.toCString(false));
                valid = false;
            }
        }
    }
    else {
        printf("2Cant get tag from '%s'\n", file_name.toCString(false));
        mp4_tag = NULL;
        valid = false;
    }
}


Mp4Info::~Mp4Info() {
}


bool Mp4Info::load(void) {
    if(Info::load()) {
        if(mp4_tag && !mp4_tag->isEmpty()) {
            if(!taglib_tag)
                load_base_tags((Tag *)mp4_tag);
            if(mp4_tag->itemListMap().contains(ALBUM_ARTIST)) {
                album_artist = mp4_tag->itemListMap()[ALBUM_ARTIST].toStringList().front();
            }
            if(mp4_tag->itemListMap().contains(TRACKNUMBER) ) { //&& !mp4_tag->itemListMap()[TRACKNUMBER].isValid()
                track_number = mp4_tag->itemListMap()[TRACKNUMBER].toIntPair().first;
                track_count  = mp4_tag->itemListMap()[TRACKNUMBER].toIntPair().second;
            }
            if(mp4_tag->itemListMap().contains(COMPOSER)) {
                composer = mp4_tag->itemListMap()[COMPOSER].toStringList().front();
            }
            if(mp4_tag->itemListMap().contains(ORIGINAL_ARTIST)) {
                original_artist = mp4_tag->itemListMap()[ORIGINAL_ARTIST].toStringList().front();
            }
            if(mp4_tag->itemListMap().contains(COPYRIGHT)) {
                copyright = mp4_tag->itemListMap()[COPYRIGHT].toStringList().front();
            }
            if(mp4_tag->itemListMap().contains(ENCODER)) {
                encoder = mp4_tag->itemListMap()[ENCODER].toStringList().front();
            }
            if(mp4_tag->itemListMap().contains(HOMEPAGE)) {
                homepage = mp4_tag->itemListMap()[HOMEPAGE].toStringList().front();
            }
            if(mp4_tag->itemListMap().contains(VOLUME)) {
                volume_number = mp4_tag->itemListMap()[VOLUME].toIntPair().first;
                volume_count = mp4_tag->itemListMap()[VOLUME].toIntPair().second;
            }
            if(mp4_tag->itemListMap().contains(COMPILATION)) {
                is_compilation = mp4_tag->itemListMap()[COMPILATION].toBool();
            }
            // Rating
            if(mp4_tag->itemListMap().contains(RATING)) {
                int rat = 0;
                rat = mp4_tag->itemListMap()[RATING].toStringList().front().toInt();
                if(rat) {
                    if(rat > 5)
                        rating = popularity_to_rating(rat);
                    else
                        rating = rat;
                }
            }
            if(mp4_tag->itemListMap().contains(BPM_TAG)) {
                beats_per_minute = mp4_tag->itemListMap()[BPM_TAG].toInt();
            }
            if(mp4_tag->itemListMap().contains(PLAYCOUNT)) {
                playcount = mp4_tag->itemListMap()[PLAYCOUNT].toInt();
            }
            // Labels
            if(track_labels.size() == 0) {
                if(mp4_tag->itemListMap().contains(TRACK_LABELS)) {
                    track_labels_string = mp4_tag->itemListMap()[TRACK_LABELS].toStringList().front();
                    track_labels = split(track_labels_string, "|");
                }
            }
            if(artist_labels.size() == 0) {
                if(mp4_tag->itemListMap().contains(ARTIST_LABELS)) {
                    artist_labels_string = mp4_tag->itemListMap()[ARTIST_LABELS].toStringList().front();
                    artist_labels = split(artist_labels_string, "|");
                }
            }
            if(album_labels.size() == 0) {
                if(mp4_tag->itemListMap().contains(ALBUM_LABELS)) {
                    album_labels_string = mp4_tag->itemListMap()[ALBUM_LABELS].toStringList().front();
                    album_labels = split(album_labels_string, "|");
                }
            }
            if(mp4_tag->itemListMap().contains(COVER_IMAGES))
                has_image = true;
            else
                has_image = false;
            return true;
        }
    }
    return false;
}


void mp4_check_label_frame(MP4::Tag * mp4tag, const char * description, const String &value) {
    if(mp4tag->itemListMap().contains(description)) {
            if(!value.isEmpty()) {
            mp4tag->itemListMap()[ description ] = MP4::Item(StringList(value));
        }
        else {
            mp4tag->itemListMap().erase(description);
        }
    }
    else {
            if(!value.isEmpty()) {
            mp4tag->itemListMap().insert(description, MP4::Item(StringList(value)));
        }
    }
}


bool Mp4Info::save(void) {
    if(mp4_tag) {
        if(changedflag) {
            if(changedflag & CHANGED_DATA_ALBUMARTIST)
                mp4_tag->itemListMap()[ALBUM_ARTIST] = StringList(album_artist);
            if(changedflag & CHANGED_COMPOSER_TAG)
                mp4_tag->itemListMap()[COMPOSER] = StringList(composer);
            if(changedflag & CHANGED_ORIGINALARTIST_TAG)
                mp4_tag->itemListMap()[ORIGINAL_ARTIST] = StringList(original_artist);
            if(changedflag & CHANGED_TRACK_NUMBER) {
                if(!(changedflag & CHANGED_TRACK_COUNT)) {
                    // prevent overwrite in save_base_tags()
                    changedflag &= ~(CHANGED_TRACK_NUMBER);
                    mp4_tag->setTrack(track_number);
                }
                else {
                    // prevent overwrite in save_base_tags()
                    changedflag &= ~(CHANGED_TRACK_NUMBER);
                    changedflag &= ~(CHANGED_TRACK_COUNT);
                    mp4_tag->itemListMap()[TRACKNUMBER] = MP4::Item(track_number, track_count);
                }
            }
            if(changedflag & CHANGED_TRACK_COUNT) {
                // prevent overwrite in save_base_tags()
                changedflag &= ~(CHANGED_TRACK_NUMBER);
                changedflag &= ~(CHANGED_TRACK_COUNT);
                mp4_tag->itemListMap()[TRACKNUMBER] = MP4::Item(track_number, track_count);
            }
            if(changedflag & CHANGED_DATA_VOL_NUM) {
                mp4_tag->itemListMap()[VOLUME] = MP4::Item(volume_number, volume_count);
            }
            if(changedflag & CHANGED_IS_COMPILATION_TAG)
                mp4_tag->itemListMap()[COMPILATION] = MP4::Item(is_compilation);
            if(changedflag & CHANGED_DATA_RATING) {
                mp4_tag->itemListMap().insert(RATING, MP4::Item(String::number(rating_to_popularity(rating))));
            }
            if(changedflag & CHANGED_DATA_PLAYCOUNT) {
                mp4_tag->itemListMap().insert(PLAYCOUNT, MP4::Item(playcount));
            }
            if(changedflag & CHANGED_BPM_TAG) {
                mp4_tag->itemListMap().insert(BPM_TAG, MP4::Item(beats_per_minute));
            }
            // The Labels
            if(changedflag & CHANGED_ARTIST_LABELS)
                mp4_check_label_frame(mp4_tag, ARTIST_LABELS, artist_labels_string);
            if(changedflag & CHANGED_ALBUM_LABELS)
                mp4_check_label_frame(mp4_tag, ALBUM_LABELS, album_labels_string);
            if(changedflag & CHANGED_TRACK_LABELS)
                mp4_check_label_frame(mp4_tag, TRACK_LABELS, track_labels_string);
            
            
            if(changedflag & CHANGED_COPYRIGHT_TAG)
                mp4_tag->itemListMap()[COPYRIGHT] = StringList(copyright);
            if(changedflag & CHANGED_ENCODER_TAG)
                mp4_tag->itemListMap()[ENCODER] = StringList(encoder);
            if(changedflag & CHANGED_HOMEPAGE_TAG)
                mp4_tag->itemListMap()[HOMEPAGE] = StringList(homepage);
            
            save_base_tags((Tag *)mp4_tag);
        }
    }
    return Info::save();
}


Image::FileType get_file_type_from_format(MP4::CoverArt::Format format) {
    switch(format) {
        case MP4::CoverArt::JPEG:
            return Image::TYPE_JPEG;
        case MP4::CoverArt::PNG:
            return Image::TYPE_PNG;
        case MP4::CoverArt::BMP:
            return Image::TYPE_BMP;
        case MP4::CoverArt::GIF:
            return Image::TYPE_GIF;
        default:
            return Image::TYPE_UNKNOWN;
    }
}

MP4::CoverArt::Format get_format_from_image_file_type(Image::FileType image_file_type) {
    switch(image_file_type) {
        case Image::TYPE_JPEG:
            return MP4::CoverArt::JPEG;
            break;
        case Image::TYPE_PNG:
            return MP4::CoverArt::PNG;
            break;
        case Image::TYPE_BMP:
            return MP4::CoverArt::BMP;
            break;
        case Image::TYPE_GIF:
            return MP4::CoverArt::GIF;
            break;
        default:
            return MP4::CoverArt::JPEG;
    }
}


ImageList Mp4Info::get_images() const {
    ImageList images;
    if(mp4_tag) {
        if(mp4_tag && mp4_tag->itemListMap().contains(COVER_IMAGES)) {
            MP4::CoverArtList covers = mp4_tag->itemListMap()[ COVER_IMAGES ].toCoverArtList();
            int cnt = covers.size();
            Image * image = NULL;
            for(MP4::CoverArtList::Iterator it = covers.begin(); it != covers.end(); it++) {
                image = new Image();
                image->set_content_type(Image::CONTENT_OTHER);
                image->set_data(it->data());
                image->set_file_type(get_file_type_from_format(it->format()));
                if(!it->data().isEmpty())
                    images.append(image);
                else
                    delete image;
            }
        }
    }
    return images;
}


void Mp4Info::set_images(const ImageList images) {
    if(mp4_tag) {
        if(mp4_tag->itemListMap().contains(COVER_IMAGES)) {
            mp4_tag->itemListMap().erase(COVER_IMAGES);
        }
        MP4::CoverArtList cover_list;
        for(ImageList::ConstIterator it = images.begin(); it != images.end(); ++it) {
            const Image * image = *it;
            if(!image->get_data().isEmpty()) {
                ByteVector image_data = image->get_data();
                MP4::CoverArt::Format format = get_format_from_image_file_type(image->get_file_type());
                MP4::CoverArt cover(format, image_data);
                cover_list.append(cover);
            }
        }
        if(0 < cover_list.size())
            mp4_tag->itemListMap()[ COVER_IMAGES ] = cover_list;
    }
}


String Mp4Info::get_lyrics(void) const {
    if(mp4_tag) {
        if(mp4_tag->itemListMap().contains(LYRICS))
            return mp4_tag->itemListMap()[ LYRICS ].toStringList().front();
    }
    return String();
}


void Mp4Info::set_lyrics(const String &lyrics) {
    if(mp4_tag) {
        if(mp4_tag->itemListMap().contains(LYRICS))
            mp4_tag->itemListMap().erase(LYRICS);
        if(!lyrics.isEmpty()) 
            mp4_tag->itemListMap()[ LYRICS ] = StringList(lyrics);
    }
}





/*
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 
 */


#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_modtags.h"


#include <s3mfile.h>
#include <modtag.h>

using namespace TagInfo;



S3mInfo::S3mInfo(const String &filename) : ModTagInfo(filename) {
    if(file_name.isEmpty()) {
        valid = false;
        printf("File name empty!\n");
    }
    else {
        taglib_file = new S3M::File(file_name.toCString(false), true, AudioProperties::Fast);
    }
    if(taglib_file) {
        if(!taglib_file->isOpen()) {
            cout << "Cannot open file '" << file_name << "'" << endl;
            valid = false;
            return;
        }
        taglib_tagMod = (Mod::Tag *)((S3M::File *) taglib_file)->tag();
        if(!taglib_tagMod || taglib_tagMod->isEmpty()) { 
            taglib_tag = ((S3M::File *) taglib_file)->tag();
            if(!taglib_tag) {
                printf("Cant get tag object from '%s'\n", file_name.toCString(false));
                valid = false;
            }
        }
    }
    else {
        printf("Cant get tag from '%s'\n", file_name.toCString(false));
        taglib_tagMod = NULL;
        valid = false;
    }
}


S3mInfo::~S3mInfo() {
}


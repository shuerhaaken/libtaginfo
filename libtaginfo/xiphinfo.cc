/*
 * Copyright (C) 2008-2013 J.Rios <anonbeat@gmail.com>
 * Copyright (C) 2013 Jörn Magens
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
 * Floor, Boston, MA  02110-1301  USA
 * https://www.gnu.org/licenses/lgpl-2.1.txt
 *
 * Author:
 * 	Jörn Magens <shuerhaaken@googlemail.com>
 * 	Matias De lellis <mati86dl@gmail.com>
 * 	Pavel Vasin <rat4vier@gmail.com>
 */


#include "taginfo.h"
#include "taginfo_internal.h"
#include "taginfo_xiphtags.h"


#include <string>
#include <xiphcomment.h>




using namespace TagInfo;


#define LYRICS                  "LYRICS"
#define COVERARTMIME            "COVERARTMIME"
#define COVERART                "COVERART"
#define COVERARTDESCRIPTION     "COVERARTDESCRIPTION"
#define COVERARTTYPE            "COVERARTTYPE"
#define DISCNUMBER              "DISCNUMBER"
#define DISCTOTAL               "DISCTOTAL"
#define COMPOSER                "COMPOSER"
#define PERFORMER               "PERFORMER"
#define TRACKTOTAL              "TRACKTOTAL"
#define COMPILATION             "COMPILATION"
#define ALBUMARTIST             "ALBUMARTIST"
#define RATING                  "RATING"
#define PLAY_COUNTER            "PLAY_COUNTER"
#define TRACK_LABELS            "TRACK_LABELS"
#define ALBUM_LABELS            "ALBUM_LABELS"
#define ARTIST_LABELS           "ARTIST_LABELS"
#define COPYRIGHT               "COPYRIGHT"
#define ENCODED_BY              "ENCODED-BY"  
#define LICENSE                 "LICENSE"
#define BPM_TAG                 "BPM"

static const string base64_char_string = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";


static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}


inline string base64_decode(const char* encoded_string) {
    int in_len = strlen(encoded_string);// encoded_string.size();
    int i = 0;
    int j = 0;
    int in_ = 0;
    unsigned char char_array_4[4], char_array_3[3];
    string ret;
    
    while(in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
        char_array_4[i++] = encoded_string[in_]; in_++;
        if (i ==4) {
        for (i = 0; i <4; i++)
            char_array_4[i] = base64_char_string.find(char_array_4[i]);
        
        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];
        
        for (i = 0; (i < 3); i++)
            ret += char_array_3[i];
            i = 0;
        }
    }
    
    if(i) {
        for (j = i; j <4; j++)
            char_array_4[j] = 0;
        
        for (j = 0; j <4; j++)
            char_array_4[j] = base64_char_string.find(char_array_4[j]);
        
        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];
        
        for (j = 0; (j < i - 1); j++)
            ret += char_array_3[j];
    }
    return ret;
}


static const char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char base64_pad = '=';

inline int base64encode_internal(const char * src, const size_t srclen, char * dst, const size_t dstlen) {
    if(((srclen / 3) + ((srclen % 3) > 0)) * 4 > dstlen)
        return -1;
    
    unsigned int tmp;
    const unsigned char * dat = ( unsigned char * ) src;
    int OutPos = 0;
    for(int i = 0; i < ( int ) srclen / 3; i++) {
        tmp  = (*dat++) << 16;
        tmp |= ((*dat++) <<  8);
        tmp |= (*dat++);
        dst[ OutPos++ ] = base64_chars[(tmp & 0x00FC0000 ) >> 18];
        dst[ OutPos++ ] = base64_chars[(tmp & 0x0003F000 ) >> 12];
        dst[ OutPos++ ] = base64_chars[(tmp & 0x00000FC0 ) >>  6];
        dst[ OutPos++ ] = base64_chars[(tmp & 0x0000003F )      ];
    }
    switch( srclen % 3 ) {
        case 1 :
            tmp = (* dat++) << 16;
            dst[OutPos++] = base64_chars[(tmp & 0x00FC0000 ) >> 18];
            dst[OutPos++] = base64_chars[(tmp & 0x0003F000 ) >> 12];
            dst[OutPos++] = base64_pad;
            dst[OutPos++] = base64_pad;
            break;
        case 2 :
            tmp  = (*dat++) << 16;
            tmp += (*dat++) <<  8;
            dst[OutPos++] = base64_chars[(tmp & 0x00FC0000 ) >> 18];
            dst[OutPos++] = base64_chars[(tmp & 0x0003F000 ) >> 12];
            dst[OutPos++] = base64_chars[(tmp & 0x00000FC0 ) >>  6];
            dst[OutPos++] = base64_pad;
            break;
    }
    return OutPos;
}

inline String base64encode(const char* src, const size_t srclen) {
    String RetVal;
    int dstlen = ((srclen / 3) + ((srclen % 3) > 0)) * 4;
    char * dst = (char *) malloc(dstlen);
    if(base64encode_internal(src, srclen, dst, dstlen) > 0) {
        ByteVector vect(dst, dstlen);
        RetVal = String(vect);
    }
    free(dst);
    return RetVal;
}


void check_xiph_label_frame(Ogg::XiphComment * xiphcomment, 
                                 const char * description, 
                                 const String &value) {
    if(xiphcomment->fieldListMap().contains(description)) {
            if(!value.isEmpty()) {
            xiphcomment->addField(description, value);
        }
        else {
            xiphcomment->removeField(description);
        }
    }
    else {
            if(!value.isEmpty()) {
            xiphcomment->addField(description, value);
        }
    }
}


XiphInfo::XiphInfo(const String &filename) : Info(filename) {
    xiphcomment = NULL;
}


XiphInfo::~XiphInfo() {
    //std::cout << "XiphInfo dtor" << std::endl;
}



bool XiphInfo::load(void) {
    if(Info::load()) {
        if(xiphcomment && !xiphcomment->isEmpty()) {
            if(!taglib_tag)
                load_base_tags((Tag *)xiphcomment);
            
            if(xiphcomment->fieldListMap().contains(COMPOSER)) {
                composer = xiphcomment->fieldListMap()[COMPOSER].front();
            }
            if(xiphcomment->fieldListMap().contains(PERFORMER)) {
                original_artist = xiphcomment->fieldListMap()[PERFORMER].front();
            }
            if(xiphcomment->fieldListMap().contains(DISCNUMBER)) {
                volume_number = xiphcomment->fieldListMap()[DISCNUMBER].front().toInt();
            }
            if(xiphcomment->fieldListMap().contains(DISCTOTAL)) {
                volume_count = xiphcomment->fieldListMap()[DISCTOTAL].front().toInt();
            }
            else if(xiphcomment->fieldListMap().contains("TOTALDISC")) {
                volume_count = xiphcomment->fieldListMap()["TOTALDISC"].front().toInt();
            }
            if(xiphcomment->fieldListMap().contains(BPM_TAG)) {
                beats_per_minute = xiphcomment->fieldListMap()[BPM_TAG].front().toInt();
            }
            if(xiphcomment->fieldListMap().contains(TRACKTOTAL)) {
                track_count = xiphcomment->fieldListMap()[TRACKTOTAL].front().toInt();
            }
            if(xiphcomment->fieldListMap().contains(COMPILATION)) {
                is_compilation = xiphcomment->fieldListMap()[COMPILATION].front() == String("1");
            }
            if(xiphcomment->fieldListMap().contains(ALBUMARTIST)) {
                album_artist = xiphcomment->fieldListMap()[ALBUMARTIST].front();
            }
            else if(xiphcomment->fieldListMap().contains("ALBUM ARTIST")) {
                album_artist = xiphcomment->fieldListMap()["ALBUM ARTIST"].front();
            }
            if(xiphcomment->fieldListMap().contains(COPYRIGHT)) {
                copyright = xiphcomment->fieldListMap()[COPYRIGHT].front();
            }
            if(xiphcomment->fieldListMap().contains(ENCODED_BY)) {
                encoder = xiphcomment->fieldListMap()[ENCODED_BY].front();
            }
            if(xiphcomment->fieldListMap().contains(LICENSE)) {
                homepage = xiphcomment->fieldListMap()[LICENSE].front();
            }
            // Rating
            if(xiphcomment->fieldListMap().contains(RATING)) {
                int rat = 0;
                rat = xiphcomment->fieldListMap()[RATING].front().toInt();
                if(rat) {
                    if(rat > 5)
                        rating = popularity_to_rating(rat);
                    else
                        rating = rat;
                }
            }
            if(xiphcomment->fieldListMap().contains(PLAY_COUNTER)) {
                playcount = xiphcomment->fieldListMap()[PLAY_COUNTER].front().toInt();
            }
            // Labels
            if(track_labels.size() == 0) {
                if(xiphcomment->fieldListMap().contains(TRACK_LABELS)) {
                    track_labels_string = xiphcomment->fieldListMap()[TRACK_LABELS].front();
                    track_labels = split(track_labels_string, "|");
                }
            }
            if(artist_labels.size() == 0) {
                if(xiphcomment->fieldListMap().contains(ARTIST_LABELS)) {
                    artist_labels_string = xiphcomment->fieldListMap()[ARTIST_LABELS].front();
                    artist_labels = split(artist_labels_string, "|");
                }
            }
            if(album_labels.size() == 0) {
                if(xiphcomment->fieldListMap().contains(ALBUM_LABELS)) {
                    album_labels_string = xiphcomment->fieldListMap()[ALBUM_LABELS].front();
                    album_labels = split(album_labels_string, "|");
                }
            }
            if(xiphcomment->contains(COVERART))
                has_image = true;
            return true;
        }
    }
    return false;
}


bool XiphInfo::save(void) {
    if(xiphcomment) {
        if(changedflag) {
            if(changedflag & CHANGED_DATA_VOL_NUM)
                xiphcomment->addField(DISCNUMBER, String::number(static_cast<int>(volume_number)), true);
            if(changedflag & CHANGED_DATA_VOL_CNT)
                xiphcomment->addField(DISCTOTAL, String::number(static_cast<int>(volume_count)), true);
            if(changedflag & CHANGED_COMPOSER_TAG)
                xiphcomment->addField(COMPOSER, composer, true);
            if(changedflag & CHANGED_ORIGINALARTIST_TAG)
                xiphcomment->addField(PERFORMER, original_artist, true);
            if(changedflag & CHANGED_TRACK_COUNT)
                xiphcomment->addField(TRACKTOTAL, String::number(static_cast<int>(track_count)), true);
            if(changedflag & CHANGED_IS_COMPILATION_TAG) {
                if(is_compilation) {
                    xiphcomment->addField(COMPILATION, "1", true);
                }
                else {
                    xiphcomment->addField(COMPILATION, "0", true);
                }
            }
            if(changedflag & CHANGED_DATA_ALBUMARTIST)
                xiphcomment->addField(ALBUMARTIST, album_artist, true);
            if(changedflag & CHANGED_DATA_RATING)
                xiphcomment->addField(RATING, String::number(static_cast<int>(rating_to_popularity(rating))), true);
            if(changedflag & CHANGED_DATA_PLAYCOUNT)
                xiphcomment->addField(PLAY_COUNTER, String::number(static_cast<int>(playcount)), true);
            if(changedflag & CHANGED_BPM_TAG)
                xiphcomment->addField(BPM_TAG, String::number(static_cast<int>(beats_per_minute)), true);
            
            // The Labels
            if(changedflag & CHANGED_TRACK_LABELS)
                check_xiph_label_frame(xiphcomment, TRACK_LABELS, track_labels_string);
            if(changedflag & CHANGED_ARTIST_LABELS)
                check_xiph_label_frame(xiphcomment, ARTIST_LABELS, artist_labels_string);
            if(changedflag & CHANGED_ALBUM_LABELS)
                check_xiph_label_frame(xiphcomment, ALBUM_LABELS, album_labels_string);
            
            if(changedflag & CHANGED_COPYRIGHT_TAG)
                xiphcomment->addField(COPYRIGHT, copyright, true);
            if(changedflag & CHANGED_ENCODER_TAG)
                xiphcomment->addField(ENCODED_BY, encoder, true);
            if(changedflag & CHANGED_HOMEPAGE_TAG)
                xiphcomment->addField(LICENSE, homepage, true);
            
            save_base_tags((Tag *)xiphcomment);
        }
    }
    return Info::save();
}


ImageList XiphInfo::get_images() const {
    ImageList images;
    if(xiphcomment && xiphcomment->contains(COVERART)) {
        
        StringList pics      = xiphcomment->fieldListMap()[ COVERART ];
        StringList pic_mimes = xiphcomment->fieldListMap()[ COVERARTMIME ];
        StringList pic_types = xiphcomment->fieldListMap()[ COVERARTTYPE ];
        StringList pic_descr = xiphcomment->fieldListMap()[ COVERARTDESCRIPTION ];
        
        int cnt       = pics.size();
        int mimes_cnt = pic_mimes.size();
        int types_cnt = pic_types.size();
        int descr_cnt = pic_descr.size();
        for(int i = 0; i < cnt; i++) {
            Image * image = new Image();
            String mimetype;
            String description;
            String itype;
            if(i < mimes_cnt) {
                mimetype = pic_mimes[i];//.to8Bit(false); 
                if(mimetype.find("/jpeg") != -1 || mimetype.find("/jpg") != -1)
                    image->set_file_type(Image::TYPE_JPEG);
                else if(mimetype.find("/png") != -1)
                    image->set_file_type(Image::TYPE_PNG);
                else if(mimetype.find("/gif") != -1)
                    image->set_file_type(Image::TYPE_GIF);
                else if(mimetype.find("/bmp") != -1)
                    image->set_file_type(Image::TYPE_BMP);
            }
            if(i < types_cnt) {
                itype = pic_types[i];
                image->set_content_type((Image::ContentType) itype.toInt());
            }
            if(i < descr_cnt) {
                description = pic_descr[i];
                image->set_description(description);
            }
            const char* CoverEncData = pics[i].toCString(false);
            
            string CoverDecData = base64_decode(CoverEncData); /* STANDARD STRING !! */
            
            ByteVector bv(CoverDecData.data(), CoverDecData.size());
            image->set_data(bv);
            images.append(image);
        }
    }
    return images;
}

void XiphInfo::set_images(const ImageList images) {
    if(xiphcomment) {
        xiphcomment->removeField(COVERARTMIME);
        xiphcomment->removeField(COVERART);
        xiphcomment->removeField(COVERARTTYPE);
        xiphcomment->removeField(COVERARTDESCRIPTION);
        for(ImageList::ConstIterator it = images.begin(); it != images.end(); ++it) {
            const Image * image = *it;
            if(!image->get_data().isEmpty()) {
                if(image->get_file_type() == Image::TYPE_UNKNOWN || 
                   image->get_file_type() == Image::TYPE_JPEG)
                    xiphcomment->addField(COVERARTMIME, "image/jpeg", false);
                else if(image->get_file_type() == Image::TYPE_PNG)
                    xiphcomment->addField(COVERARTMIME, "image/png", false);
                else if(image->get_file_type() == Image::TYPE_GIF)
                    xiphcomment->addField(COVERARTMIME, "image/gif", false);
                else if(image->get_file_type() == Image::TYPE_BMP)
                    xiphcomment->addField(COVERARTMIME, "image/bmp", false);
                String b64Encoded = base64encode(image->get_data().data(), image->get_data().size());
                xiphcomment->addField(COVERART, 
                                      b64Encoded.toCString(false),
                                      false);
                xiphcomment->addField(COVERARTTYPE, String::number(static_cast<int>(image->get_content_type())), false);
                
                if(!image->get_description().isEmpty())
                    xiphcomment->addField(COVERARTDESCRIPTION, image->get_description(), false);
            }
        }
    }
}


String XiphInfo::get_lyrics(void) const {
    if(xiphcomment && xiphcomment->contains(LYRICS))
        return xiphcomment->fieldListMap()[ LYRICS ].front();
    return String();
}


void XiphInfo::set_lyrics(const String &lyrics) {
    if(xiphcomment) {
        xiphcomment->removeField(LYRICS);
        if(!lyrics.isEmpty())
            xiphcomment->addField(LYRICS, lyrics, true);
    }
}


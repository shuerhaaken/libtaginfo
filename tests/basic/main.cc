#include "taginfo.h"

using namespace TagInfo;

int main( void ) {
    InfoRef iref(TESTDIR "samples/sample.flac");
    if(iref.is_valid()) {
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}

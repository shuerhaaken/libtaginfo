#include <stdio.h>
#include <stdlib.h>
#include <taginfo_c.h>

int main(void)
{
    TagInfo_Info *info;
    
    char val[200] = TESTDIR "samples/sample.flac";
    info = taginfo_info_create(val);
    if(info == NULL)
        return 1; //EXIT_FAILURE
    
    taginfo_info_enable_string_management(1);
    char* title;
    char* album;
    char* comment;
    char* composer;
    
    if(taginfo_info_load(info)) {
        title         = taginfo_info_get_title(info);
        album         = taginfo_info_get_album(info);
        comment       = taginfo_info_get_comment(info);
        composer      = taginfo_info_get_composer(info);
    }
    else {
        taginfo_info_free(info);
        return 1; //EXIT_FAILURE
    }
    //printf("%s  %s  %s  %s  ", title, album, comment, composer);
    if(strcmp(title, "FLAC title") != 0 ||
       strcmp(album, "FLAC album") != 0 ||
       strcmp(comment, "FLAC comment") != 0 ||
       strcmp(composer, "FLAC composer") != 0   ) {
        taginfo_info_free(info);
        return 1; //EXIT_FAILURE
    }
    taginfo_info_free_strings();
    taginfo_info_free(info);
    return 0; //EXIT_SUCCESS
}

#include <stdio.h>
#include <taginfo_c.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>

#define TESTTITLE       "test title äöüß"
#define DESCRIPTION     "Däß krip tiön"

int cp(const char *from, const char *to) {
    int fd_to, fd_from;
    char buf[4096];
    ssize_t nread;
    int saved_errno;
    
    fd_from = open(from, O_RDONLY);
    if (fd_from < 0)
        return -1;
    
    fd_to = open(to, O_WRONLY | O_CREAT , 0666); //| O_EXCL
    if (fd_to < 0)
        goto out_error;
    
    while (nread = read(fd_from, buf, sizeof buf), nread > 0) {
        char *out_ptr = buf;
        ssize_t nwritten;
        do {
            nwritten = write(fd_to, out_ptr, nread);
            
            if (nwritten >= 0) {
                nread -= nwritten;
                out_ptr += nwritten;
            }
            else if (errno != EINTR) {
                goto out_error;
            }
        } while (nread > 0);
    }
    if (nread == 0) {
        if (close(fd_to) < 0) {
            fd_to = -1;
            goto out_error;
        }
        close(fd_from);
        return 0; /* Success! */
    }
    
  out_error:
    saved_errno = errno;
    close(fd_from);
    if (fd_to >= 0)
        close(fd_to);
    
    errno = saved_errno;
    return -1;
}


int check_list_equal(char** labels, int labels_length, char** out_labels, int out_labels_length) {
    if(labels_length != out_labels_length)
        return 0; // not equal
    int i = 0;
    for(; i < labels_length; i++) {
        const char* first = labels[i];
        const char* second = out_labels[i];
        if(strcmp(labels[i], out_labels[i]) != 0)
            return 0; // not equal
    }
    return 1; //EQUAL
}

int main(void) {
/*    printf("**0: \n");*/
    TagInfo_Info *info;
    char val[200] = TESTDIR "samples/sample.flac";
    const char* target = "/tmp/out_c03.flac";
    const char* s = val;
    const char* image_path = TESTDIR "samples/test.jpg";
    
    //printf("##1\n");
    if(cp(s, target) < 0)
        return 1; //EXIT_FAILURE
    
    info = taginfo_info_create(target);
    
    if(info == NULL)
        return 1; //EXIT_FAILURE
    struct stat filestatus;
    int fd;
    stat( image_path, &filestatus );
    /*printf("filestatus.st_size: %d\n", (int)filestatus.st_size);*/
    
    fd = open(image_path, O_RDONLY);
    if (fd == -1) {
        perror("Error opening file for reading");
        return 1;
    }
    TagInfo_Image * img = taginfo_image_new();
    taginfo_image_set_data(img, (char*)mmap(0, filestatus.st_size, PROT_READ, MAP_SHARED, fd, 0), (unsigned int)filestatus.st_size);
    taginfo_image_set_content_type(img, TAG_INFO_IMAGE_CONTENT_OTHER);
    taginfo_image_set_description(img, strdup(DESCRIPTION));
    TagInfo_Image ** ima = malloc(1 * sizeof(img));
    *ima = img;
    
    taginfo_info_set_images(info, ima, 1);
    
    taginfo_info_set_album(info, TESTTITLE " älbum");
    taginfo_info_set_albumartist(info, TESTTITLE "äé");
    taginfo_info_set_beats_per_minute(info, 180);
    taginfo_info_set_artist(info, TESTTITLE "artist");
    taginfo_info_set_comment(info, TESTTITLE " comment");
    taginfo_info_set_composer(info, TESTTITLE " composer");
    taginfo_info_set_copyright(info, TESTTITLE " copyright");
    taginfo_info_set_encoder(info, TESTTITLE " enc");
    taginfo_info_set_genre(info, TESTTITLE " genre");
    taginfo_info_set_homepage(info, "www.äé.de");
    taginfo_info_set_is_compilation(info, 1);
    taginfo_info_set_original_artist(info, TESTTITLE " org artist");
    taginfo_info_set_playcount(info, 102);
    taginfo_info_set_rating(info, 5);
    taginfo_info_set_title(info, TESTTITLE);
    taginfo_info_set_track_count(info, 4);
    taginfo_info_set_track_number(info, 3);
    taginfo_info_set_volume_count(info, 2);
    taginfo_info_set_volume_number(info, 1);
    taginfo_info_set_year(info, 1982);
    int labels_length = 2;
    char** labels = malloc(labels_length * sizeof(char*));
    labels[0] = strdup("label 1 äöü");
    labels[1] = strdup("label 2 äéü");
    taginfo_info_set_artist_labels(info, (const char * const*)labels, labels_length);
    taginfo_info_set_album_labels(info,  (const char * const*)labels, labels_length);
    taginfo_info_set_track_labels(info,  (const char * const*)labels, labels_length);
    taginfo_info_save(info);
    taginfo_info_free(info);
    info = taginfo_info_create(target);
    int bla = 0;
    
    int j = 0;
    if(taginfo_info_load(info)) {
        printf("++1\n");
        TagInfo_Image ** iout = taginfo_info_get_images(info, &bla);
        printf("++2 %d\n", bla);
        int i;
        if(bla <= 0)
            return 1;
        TagInfo_Image * image = iout[0];
        if(!image)
            return 1;
        if(strcmp(taginfo_image_get_description(image), DESCRIPTION) != 0)
            return 1; //EXIT_FAILURE
        unsigned int len = 0;
        unsigned int org_len = 0;
        char * idat   = taginfo_image_get_data(image, &len);
        char * orgdat = taginfo_image_get_data(img, &org_len);
        if(filestatus.st_size != len)
            return 1; //EXIT_FAILURE
        for(i = 0; i < len; i++) {
            if(orgdat[i] != idat[i]) {
                return 1; //EXIT_FAILURE
            }
        }
        int out_labels_length_artist = 0;
        char** out_labels_artist = taginfo_info_get_artist_labels(info, &out_labels_length_artist);
        int out_labels_length_album = 0;
        char** out_labels_album = taginfo_info_get_album_labels(info, &out_labels_length_album);
        int out_labels_length_track = 0;
        char** out_labels_track = taginfo_info_get_track_labels(info, &out_labels_length_track);
        
        if(1 &&
           strcmp(taginfo_info_get_album(info), (TESTTITLE " älbum")) == 0 &&
           strcmp(taginfo_info_get_albumartist(info), (TESTTITLE "äé")) == 0 &&
           strcmp(taginfo_info_get_artist(info), (TESTTITLE "artist")) == 0 &&
           strcmp(taginfo_info_get_comment(info), (TESTTITLE " comment")) == 0 &&
           strcmp(taginfo_info_get_composer(info), (TESTTITLE " composer")) == 0 &&
           strcmp(taginfo_info_get_copyright(info), (TESTTITLE " copyright")) == 0 &&
           strcmp(taginfo_info_get_encoder(info), (TESTTITLE " enc")) == 0 &&
           strcmp(taginfo_info_get_genre(info), (TESTTITLE " genre")) == 0 &&
           strcmp(taginfo_info_get_homepage(info), ("www.äé.de")) == 0 &&
           taginfo_info_get_is_compilation(info) &&
           strcmp(taginfo_info_get_original_artist (info), (TESTTITLE " org artist")) == 0 &&
           taginfo_info_get_playcount(info) == 102 &&
           taginfo_info_get_beats_per_minute(info) == 180 &&
           taginfo_info_get_rating(info) == 5 &&
           strcmp(taginfo_info_get_title(info), (TESTTITLE)) == 0 &&
           taginfo_info_get_track_count(info) == 4 &&
           taginfo_info_get_track_number(info) == 3 &&
           taginfo_info_get_volume_count(info) == 2 &&
           taginfo_info_get_volume_number(info) == 1 &&
           taginfo_info_get_year(info) == 1982 &&
           check_list_equal(labels, labels_length, out_labels_artist, out_labels_length_artist) &&
           check_list_equal(labels, labels_length, out_labels_album, out_labels_length_album) &&
           check_list_equal(labels, labels_length, out_labels_track, out_labels_length_track) &&
           1) {
            
            for(j = 0; j < labels_length; j++) {
                free(labels[j]);
                free(out_labels_artist[j]);
                free(out_labels_album[j]);
                free(out_labels_track[j]);
            }
            taginfo_info_free(info);
            return 0; //EXIT_SUCCESS
        }
        for(j = 0; j < labels_length; j++) {
            free(labels[j]);
            free(out_labels_artist[j]);
            free(out_labels_album[j]);
            free(out_labels_track[j]);
        }
        taginfo_info_free(info);
        return 1; //EXIT_FAILURE
    }
    else {
        taginfo_info_free(info);
        return 1; //EXIT_FAILURE
    }
}

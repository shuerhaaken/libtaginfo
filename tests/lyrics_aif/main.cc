#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

using namespace TagInfo;

void set_lyrics(const String &target, const String &lyrics) {
    InfoRef iref(target);
    if(iref.is_valid()) {
        Info * info = iref.info();
        info->set_lyrics(lyrics);
        iref.save();
    }
}

String get_lyrics(const String &target) {
    String res;
    InfoRef iref(target);
    if(iref.is_valid()) {
        if(iref.load()) {
            Info * info = iref.info();
            res = info->get_lyrics();
        }
    }
    return res;
}

int main( void ) {
    std::string val   = TESTDIR "samples/sample.aif";
    
    const String target = "/tmp/out_99.aif";
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.toCString(false));
    dst << src.rdbuf();
    
    const String lyrics = "these are lyrics öäüß\n ldskjfslkd\n öäüß";
    
    set_lyrics(target, lyrics);
    
    String l = get_lyrics(target);

    if(lyrics != l) {
        if(remove(target.toCString(false)) != 0 ) {
            cout << "tmp file removal failed for " << target << endl;
            return EXIT_FAILURE;
        }
        return EXIT_FAILURE;
    }
    else {
        if(remove(target.toCString(false)) != 0 ) {
            cout << "tmp file removal failed for " << target << endl;
            return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
    }
}


#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

using namespace TagInfo;
//using namespace std;


int main( void ) {
    Info * info;
    std::string val   = TESTDIR "samples/sample.wma";
    
    //std::cout << std::endl << "val: " << val << std::endl;
    std::string target = "/tmp/out_01.wma";
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.c_str());
    dst << src.rdbuf();
    
    char* data;
    int data_length;
    
    struct stat filestatus;
    
    
    String lyrics = "these are lyrics öäüß\n ldskjfslkd\n öäüß";
    
    info = Info::create(target);
    if(info) {
        info->set_lyrics(lyrics);
        info->save();
    }
    delete info;
    info = NULL;
    
    info = Info::create(target);
    char* read;
    if(info) {
        if(info->load()) {
            String res = info->get_lyrics();
            if(lyrics != res) {
                delete info;
                if(remove(target.c_str()) != 0 ) {
                    return EXIT_FAILURE;
                }
            }
            if(remove(target.c_str()) != 0 )
                return EXIT_FAILURE;
            return EXIT_SUCCESS;
        }
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
    else {
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
    return EXIT_FAILURE;
}


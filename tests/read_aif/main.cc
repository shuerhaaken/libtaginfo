#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace TagInfo;


int main( void ) {
    Info * info;
    
    std::string val = TESTDIR "samples/sample.aif";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    std::string target = "/tmp/out_01.aif";
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.c_str());
    dst << src.rdbuf();
     
    info = Info::create(target);
    if( info ) {
        if( info->load() ) {
            //std::cout << "info->get_comments(): " << info->get_comments() << std::endl;
            //std::cout << "info->get_album(): " << info->get_album() << std::endl;
            //std::cout << "info->get_composer(): " << info->get_composer() << std::endl;
            //std::cout << "info->get_artist(): " << info->get_artist() << std::endl;
            if(info->get_title() == "Aiff Title" && 
               info->get_artist() == "Aiff Artist" &&
               info->get_comments() == "Aiff Comment" &&
               info->get_album() == "Aiff Album"
               ) {
                delete info;
                if(remove(target.c_str()) != 0 )
                    return EXIT_FAILURE;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
}

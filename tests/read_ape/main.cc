#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace TagInfo;


int main( void ) {
    Info * info;
    
    std::string val = TESTDIR "samples/sample.ape";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    std::string target = "/tmp/out_01.ape";
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.c_str());
    dst << src.rdbuf();
     
    info = Info::create(target);
    if( info ) {
        if( info->load() ) {
            //std::cout << "info->title: " << info->get_title() << std::endl;
            //std::cout << "info->artist: " << info->get_artist() << std::endl;
            //std::cout << "info->album: " << info->get_album() << std::endl;
            //std::cout << "info->comments: " << info->get_comments() << std::endl;
            if(info->get_title() == "APE title" &&
               info->get_artist() == "APE artist" &&
               info->get_album() == "APE album" &&
               info->get_comments() == "APE comment"
               ) {
                delete info;
                if(remove(target.c_str()) != 0 )
                    return EXIT_FAILURE;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
}

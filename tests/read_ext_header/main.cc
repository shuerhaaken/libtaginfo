#include "taginfo.h"

using namespace TagInfo;

int main( void ) {
    Info * info;
    std::string val = TESTDIR "samples/sample_v2_3_ext_header.mp3";
    
    //std::cout << std::endl << "val: " << val << std::endl;
    info = Info::create( val );
    if( info ) {
        if( info->load() ) {
            std::cout << "info->get_title(): " << info->get_title() << std::endl;
            std::cout << "info->get_artist(): " << info->get_artist() << std::endl;
            std::cout << "info->get_album(): " << info->get_album() << std::endl;
            std::cout << "info->get_comments(): " << info->get_comments() << std::endl;
            if(info->get_title() == "Title v1" &&
               info->get_artist() == "Interpret v1" &&
               info->get_album() == "Album v1" &&
               info->get_comments() == "Comment v1"
               ) {
                delete info;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        return EXIT_FAILURE;
    }
}

#include "taginfo.h"

using namespace TagInfo;

int main( void ) {
    Info * info;
    std::string val = TESTDIR "samples/sample.m4a";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    info = Info::create(val);
    if(info) {
        if( info->load() ) {
            //std::cout << "info->title: " << info->title << std::endl;
            if(info->get_title() == "M4A title" &&
               info->get_artist() == "M4A artist" &&
               info->get_album() == "M4A album" &&
               info->get_comments() == "M4A comment"
            ) {
                delete info;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        return EXIT_FAILURE;
    }
}

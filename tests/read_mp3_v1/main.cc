#include "taginfo.h"

using namespace TagInfo;

int main( void ) {
    Info * info;
    std::string val = TESTDIR "samples/sample_v1_only.mp3";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    info = Info::create( val );
    if( info ) {
        if( info->load() ) {
            if(info->get_title() == "MP3 title" &&
               info->get_artist() == "MP3 artist" &&
               info->get_album() == "MP3 album" &&
               info->get_comments() == "MP3 comment"
            ) {
                delete info;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        return EXIT_FAILURE;
    }
}

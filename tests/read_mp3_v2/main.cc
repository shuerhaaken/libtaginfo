#include "taginfo.h"

using namespace TagInfo;

int main( void ) {
    Info * info;
    std::string val = TESTDIR "samples/sample_v2_only.mp3";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    info = Info::create( val );
    if( info ) {
        if( info->load() ) {
            //std::cout << "info->get_title: " << info->get_title() << std::endl;
            //std::cout << "info->artist: " << info->artist << std::endl;
            //std::cout << "info->album: " << info->album << std::endl;
            if(info->get_title() == "MP3 title" &&
               info->get_artist() == "MP3 artist" &&
               info->get_album() == "MP3 album" &&
               info->get_comments() == "MP3 comment"
            ) {
                delete info;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        return EXIT_FAILURE;
    }
}

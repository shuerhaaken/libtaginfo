#include "taginfo.h"

using namespace TagInfo;

int main( void ) {
    Info * info;
    std::string val = TESTDIR "samples/sample.mpc";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    info = Info::create( val );
    if( info ) {
        if( info->load() ) {
            //std::cout << "info->title: " << info->title << std::endl;
            if(info->get_title() == "MPC title") {
                delete info;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        return EXIT_FAILURE;
    }
}

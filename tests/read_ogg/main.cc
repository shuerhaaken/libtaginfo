#include "taginfo.h"

using namespace TagInfo;

int main( void ) {
    Info * info;
    std::string val = TESTDIR "samples/sample.ogg";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    info = Info::create( val );
    if( info ) {
        if( info->load() ) {
            //std::cout << "2info->title: " << info->title << std::endl;
            if(info->get_title() == "OGG title") {
                delete info;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        return EXIT_FAILURE;
    }
}

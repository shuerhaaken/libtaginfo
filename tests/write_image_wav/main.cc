#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

using namespace TagInfo;


int main( void ) {
    Info * info;
    std::string val   = TESTDIR "samples/sample.wav";
    std::string image = TESTDIR "samples/test.jpg";
    std::string target = "/tmp/out_01.wav";
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.c_str());
    dst << src.rdbuf();
    
    Image * img = new Image();
    img->set_file_type(Image::TYPE_JPEG);
    img->set_content_type(Image::CONTENT_COVER_FRONT);
    img->set_description("Bescräibunk");
    
    Image * img2 = new Image();
    img2->set_file_type(Image::TYPE_JPEG);
    img2->set_content_type(Image::CONTENT_COVER_BACK);
    img2->set_description("Beßcräibunk");

    struct stat filestatus;
    stat( image.c_str(), &filestatus );
    //cout << filestatus.st_size << " bytes\n";
    int fd;
    
    fd = open(image.c_str(), O_RDONLY);
    if (fd == -1) {
        perror("Error opening file for reading");
        EXIT_FAILURE;
    }
    ByteVector vect = ByteVector((char*)mmap(0, filestatus.st_size, PROT_READ, MAP_SHARED, fd, 0), (uint)filestatus.st_size);
    ByteVector vect2 = ByteVector((char*)mmap(0, filestatus.st_size, PROT_READ, MAP_SHARED, fd, 0), (uint)filestatus.st_size);
    
    img->set_data(vect);
    img2->set_data(vect2);
    
    //std::cout <<  "++1 " << std::endl;
    info = Info::create(target);
    if(info) {
        ImageList imgs;
        imgs.append(img);
        imgs.append(img2);
        info->set_images(imgs);
        info->save();
    }
    delete info;
    info = NULL;
    
    info = Info::create(target);
    Image * read_image;
    Image * read_image2;
    if(info) {
        if(info->load()) {
            int len = 0;
            ImageList images = info->get_images();
            images.setAutoDelete(true);
            if(images.isEmpty()) {
                delete info;
                if(remove(target.c_str()) != 0 ) {
                    return EXIT_FAILURE;
                }
                return EXIT_FAILURE;
            }
            read_image  = images[0];
            read_image2 = images[1];
            if(!images[0] || !read_image|| !read_image2) {
                delete info;
                if(remove(target.c_str()) != 0 ) {
                    return EXIT_FAILURE;
                }
            }
            if(img->get_content_type() != read_image->get_content_type()) {
                if(read_image)
                    delete read_image;
                return EXIT_FAILURE;
            }
            if(img->get_description() != read_image->get_description()) {
                if(read_image)
                    delete read_image;
                return EXIT_FAILURE;
            }
            if(img2->get_description() != read_image2->get_description()) {
                if(read_image)
                    delete read_image;
                return EXIT_FAILURE;
            }
            if(img2->get_content_type() != read_image2->get_content_type()) {
                if(read_image)
                    delete read_image;
                return EXIT_FAILURE;
            }
            int i;
            if(img->get_data() != read_image->get_data()) {
                return EXIT_FAILURE;
            }
            if(img2->get_data() != read_image2->get_data()) {
                return EXIT_FAILURE;
            }
            if(remove(target.c_str()) != 0 )
                return EXIT_FAILURE;
            return EXIT_SUCCESS;
        }
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
    else {
       delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
    return EXIT_FAILURE;
}



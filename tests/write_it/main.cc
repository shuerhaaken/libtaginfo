#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>

#define TESTNAME "xyz ü1 23"

using namespace TagInfo;



int main( void ) {
    Info * info;
    std::string val = TESTDIR "samples/sample.it";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    std::string target("/tmp/out_01.it");
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.c_str());
    dst << src.rdbuf();
    
    info = Info::create(target);
    
    if( info ) {
        info->set_artist("artißt");
        info->set_genre("gen re");
        info->set_composer("cömpozér");
        info->set_year(1982);
        info->set_rating(0);
        info->set_title("titlöe");
        info->set_original_artist ("ürgar t");
        info->set_homepage("ööö");
        info->set_encoder("encödr");
        info->set_track_number(2);
        info->set_track_count(5);
        info->set_volume_number(1);
        info->set_volume_count(3);
        info->set_copyright("Cöpppyright");
        info->set_album_artist("äöé");
        info->set_is_compilation(true);
        info->set_comments("Cömmméé\nèntß");
        info->save();
    }
    delete info;
    info = NULL;
    
    info = Info::create(target);
    if( info ) {
        if( info->load() ) {
            if(info->get_title() == "titlöe" &&
               info->get_comments() == "Cömmméé\nèntß") {
                delete info;
                if(remove(target.c_str()) != 0 )
                    return EXIT_FAILURE;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
    else {
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
}

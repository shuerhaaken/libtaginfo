#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>

#define TESTNAME "xyz 1 23"

using namespace TagInfo;

bool check_list_equal(const StringList &list, const StringList &second) {
    //std::cout <<  "first : " << list.toString()   << std::endl;
    //std::cout <<  "second: " << second.toString() << std::endl;
    return(list.toString("") == second.toString(""));
}

int main( void ) {
    std::string val = TESTDIR "samples/sample_v2_only.mp3";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    String target("/tmp/out_09.mp3");
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.toCString(false));
    dst << src.rdbuf();
    
    InfoRef iref(target);
    
    StringList trlabels = StringList();
    trlabels.append("user label1 äöüé");
    trlabels.append("Träckß");
    trlabels.append("user label1 äiiüé");
    
    StringList allabels = StringList();
    allabels.append("user label1 äöüé");
    allabels.append("Älmúm");
    allabels.append("user label1 äiiüé");
    
    StringList arlabels = StringList();
    arlabels.append("user label1 äöüé");
    arlabels.append("Ärtißztá");
    arlabels.append("user label1 äiiüé");
    
    if( iref.is_valid() ) {
        Info * info = iref.info();
        info->set_artist("artißt");
        info->set_genre("gen re");
        info->set_composer("cömpozér");
        info->set_year(1982);
        info->set_rating(2);
        info->set_beats_per_minute(180);
        info->set_title("titlöe");
        info->set_original_artist ("ürgar t");
        info->set_homepage("ööö");
        info->set_encoder("encödr");
        info->set_track_number(2);
        info->set_track_count(5);
        info->set_volume_number(1);
        info->set_volume_count(3);
        info->set_copyright("Cöpppyright");
        info->set_album_artist("äöé");
        info->set_is_compilation(true);
        info->set_comments("Cömmméé\nèntß");
        info->set_album_labels_list(allabels);
        info->set_artist_labels_list(arlabels);
        info->set_track_labels_list(trlabels);
        iref.save();
    }
    InfoRef iref2(target);
    if( iref2.is_valid() ) {
        if( iref2.load() ) {
            Info * info = iref2.info();
            //std::cout << "info->get_track_number(): " << info->get_track_number() << std::endl;
            //std::cout << "info->get_track_count(): " << info->get_track_count() << std::endl;
            if(info->get_artist() == "artißt" &&
               info->get_genre() == "gen re" &&
               info->get_composer() == "cömpozér" &&
               info->get_year() == 1982 &&
               info->get_rating() == 2 &&
               info->get_beats_per_minute() == 180 &&
               info->get_title() == "titlöe" &&
               info->get_original_artist () == "ürgar t" &&
               info->get_homepage() == "ööö" &&
               info->get_encoder() == "encödr" &&
               info->get_track_number() == 2 &&
               info->get_track_count() == 5 &&
               info->get_volume_number() == 1 &&
               info->get_volume_count() == 3 &&
               info->get_copyright() == "Cöpppyright" &&
               info->get_album_artist() == "äöé" &&
               info->get_is_compilation() &&
               info->get_comments() == "Cömmméé\nèntß" &&
               check_list_equal(info->get_artist_labels_list(), arlabels) &&
               check_list_equal(info->get_album_labels_list(), allabels) &&
               check_list_equal(info->get_track_labels_list(), trlabels)    ) {
                if(remove(target.toCString(false)) != 0 )
                    return EXIT_FAILURE;
                return EXIT_SUCCESS;
            }
        }
        if(remove(target.toCString(false)) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
    else {
        if(remove(target.toCString(false)) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
}

#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>

#define TESTNAME "xyz 1 23"

using namespace TagInfo;

bool check_list_equal(const StringList &list, const StringList &second) {
    //std::cout <<  "first : " << list.toString()   << std::endl;
    //std::cout <<  "second: " << second.toString() << std::endl;
    return(list.toString("") == second.toString(""));
}

int main( void ) {
    Info * info;
    std::string val = TESTDIR "samples/sample_empty.spx";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    std::string target = "/tmp/out_01.spx";
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.c_str());
    dst << src.rdbuf();
    
    StringList labels = StringList();
    labels.append("user label1 äöüé");
    labels.append("user label1 äiiüé");
    
    info = Info::create(target);
    if( info ) {
        info->set_artist("artißt");
        info->set_genre("gen re");
        info->set_composer("cömpozér");
        info->set_year(1982);
        info->set_rating(0);
        info->set_title("titlöe");
        info->set_original_artist ("ürgar t");
        info->set_homepage("ööö");
        info->set_encoder("encödr");
        info->set_track_number(2);
        info->set_track_count(5);
        info->set_volume_number(1);
        info->set_volume_count(3);
        info->set_copyright("Cöpppyright");
        info->set_album_artist("äöé");
        info->set_is_compilation(true);
        info->set_comments("Cömmméé\nèntß");
        info->set_album_labels_list(labels);
        info->set_artist_labels_list(labels);
        info->set_track_labels_list(labels);
        info->save();
    }
    delete info;
    info = NULL;
    
    info = Info::create(target);
    if( info ) {
        if( info->load() ) {
            if(info->get_artist() == "artißt" &&
               info->get_genre() == "gen re" &&
               info->get_composer() == "cömpozér" &&
               info->get_year() == 1982 &&
               info->get_title() == "titlöe" &&
               info->get_original_artist () == "ürgar t" &&
               info->get_homepage() == "ööö" &&
               info->get_encoder() == "encödr" &&
               info->get_track_number() == 2 &&
               info->get_track_count() == 5 &&
               info->get_volume_number() == 1 &&
               info->get_volume_count() == 3 &&
               info->get_copyright() == "Cöpppyright" &&
               info->get_album_artist() == "äöé" &&
               info->get_is_compilation() &&
               info->get_comments() == "Cömmméé\nèntß" &&
               check_list_equal(info->get_artist_labels_list(), labels) &&
               check_list_equal(info->get_album_labels_list(), labels) &&
               check_list_equal(info->get_track_labels_list(), labels) ) {
                delete info;
                if(remove(target.c_str()) != 0 )
                    return EXIT_FAILURE;
                return EXIT_SUCCESS;
            }
        }
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
    else {
        delete info;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
}
